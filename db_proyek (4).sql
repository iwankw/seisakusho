-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2020 at 07:00 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_proyek`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `customer` varchar(100) NOT NULL,
  `alamat` varchar(2000) NOT NULL,
  `status` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `customer`, `alamat`, `status`) VALUES
(7, 'PT. AKEBONO BRAKE ASTRA INDONESIA', 'Jl. Pegangsaan Dua No.55, RT.3/RW.4, Pegangsaan Dua, Kec. Klp. Gading, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14250', 'Aktif'),
(8, 'PT. TOKAI RUBBER INDONESIA', 'Jl. Mitra Tim. II No.13 & 14, Parungmulya, Kec. Ciampel, Kabupaten Karawang, Jawa Barat 41363', 'Aktif'),
(9, 'PT. ATSUMITEC INDONESIA', 'Kawasan Industri Suryacipta, Jl. Surya Madya Kav. 1-29 A-F, Kutanegara, Kec. Ciampel, Kabupaten Karawang, Jawa Barat 41361', 'Aktif'),
(10, 'PT. PAPERTECH INDONESIA', 'Blok Tarikolot Kecamatan Cipeundeuy, Cipeundeuy, Kec. Cipeundeuy, Kabupaten Subang, Jawa Barat 41272', 'Aktif'),
(11, 'PT. YASUNLI ABADI UTAMA PLASTIC', 'Jl. H. Tabri, RT 01/02 & RT 02/02, Cirarab, Cirarab, Kec. Legok, Tangerang, Banten 15820', 'Aktif'),
(12, 'PT. DELTA MEKATRONIK', 'Jl. Kamper Delta Technologi Senter No 3, Lippo Cikarang, Kec. Cikarang Selatan, Sukaresmi, Cikarang Sel., Kab. Bekasi, Jawa Barat 17550', 'Aktif'),
(14, 'PT. SEBASTIAN JAYA METAL', 'Blok F Jalan Jababeka X Kawasan Industri Jababeka I No, RT.5, Harja Mekar, Kec. Cikarang Utara, Bekasi, Jawa Barat 17530', 'Aktif'),
(15, 'PT SEKISUI POLYMATECH INDONESIA', 'Kawasan MM2100 - Ejip Bridge, Jl. Lombok 2 No.9, Mekarwangi, Kec. Cikarang Bar., Bekasi, Jawa Barat 17530', 'Aktif'),
(16, 'PT. HADAIDA', 'Jl. Kampung Bulu, Ds. Setia Mekar, Tambun Selatan, Setiamekar, Bekasi, Jawa Barat 17510', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `estimasi`
--

CREATE TABLE `estimasi` (
  `id` int(11) NOT NULL,
  `urutan` int(11) DEFAULT NULL,
  `part` int(11) DEFAULT NULL,
  `material_fabrikasi` int(11) DEFAULT NULL,
  `proses` int(11) NOT NULL,
  `jam` int(11) NOT NULL,
  `total` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `estimasi`
--

INSERT INTO `estimasi` (`id`, `urutan`, `part`, `material_fabrikasi`, `proses`, `jam`, `total`) VALUES
(1234, NULL, 51, NULL, 4, 0, '0'),
(1235, NULL, 51, NULL, 5, 0, '0'),
(1236, NULL, 51, NULL, 6, 0, '0'),
(1237, NULL, 51, NULL, 7, 0, '0'),
(1238, NULL, 51, NULL, 8, 0, '0'),
(1239, NULL, 51, NULL, 10, 0, '0'),
(1240, NULL, 51, NULL, 11, 0, '0'),
(1241, NULL, 51, NULL, 2, 0, '2'),
(1242, NULL, 51, NULL, 18, 0, '0'),
(1243, NULL, 51, NULL, 19, 0, '0'),
(1244, NULL, 51, NULL, 20, 0, '0'),
(1245, NULL, 51, NULL, 17, 0, '0'),
(1246, NULL, 51, NULL, 16, 0, '0'),
(1247, NULL, 51, NULL, 14, 0, '0'),
(1248, NULL, 51, NULL, 13, 0, '0'),
(1249, NULL, 51, NULL, 12, 0, '0'),
(1250, NULL, 51, NULL, 9, 0, '0'),
(1251, NULL, 51, NULL, 21, 0, '0'),
(1252, NULL, 52, NULL, 4, 0, '0'),
(1253, NULL, 52, NULL, 5, 0, '0'),
(1254, NULL, 52, NULL, 6, 0, '0'),
(1255, NULL, 52, NULL, 7, 0, '0'),
(1256, NULL, 52, NULL, 8, 0, '0'),
(1257, NULL, 52, NULL, 10, 0, '0'),
(1258, NULL, 52, NULL, 11, 0, '0'),
(1259, NULL, 52, NULL, 2, 0, '0'),
(1260, NULL, 52, NULL, 18, 0, '0'),
(1261, NULL, 52, NULL, 19, 0, '0'),
(1262, NULL, 52, NULL, 20, 0, '0'),
(1263, NULL, 52, NULL, 17, 0, '0'),
(1264, NULL, 52, NULL, 16, 0, '0'),
(1265, NULL, 52, NULL, 14, 0, '0'),
(1266, NULL, 52, NULL, 13, 0, '0'),
(1267, NULL, 52, NULL, 12, 0, '0'),
(1268, NULL, 52, NULL, 9, 0, '0'),
(1269, NULL, 52, NULL, 21, 0, '0'),
(1270, NULL, 53, NULL, 4, 0, '0'),
(1271, NULL, 53, NULL, 5, 0, '0'),
(1272, NULL, 53, NULL, 6, 0, '0'),
(1273, NULL, 53, NULL, 7, 0, '0'),
(1274, NULL, 53, NULL, 8, 0, '0'),
(1275, NULL, 53, NULL, 10, 0, '0'),
(1276, NULL, 53, NULL, 11, 0, '0'),
(1277, NULL, 53, NULL, 2, 0, '0'),
(1278, NULL, 53, NULL, 18, 0, '0'),
(1279, NULL, 53, NULL, 19, 0, '0'),
(1280, NULL, 53, NULL, 20, 0, '0'),
(1281, NULL, 53, NULL, 17, 0, '0'),
(1282, NULL, 53, NULL, 16, 0, '0'),
(1283, NULL, 53, NULL, 14, 0, '0'),
(1284, NULL, 53, NULL, 13, 0, '0'),
(1285, NULL, 53, NULL, 12, 0, '0'),
(1286, NULL, 53, NULL, 9, 0, '0'),
(1287, NULL, 53, NULL, 21, 0, '0'),
(1288, NULL, 54, NULL, 4, 0, '0'),
(1289, NULL, 54, NULL, 5, 0, '0'),
(1290, NULL, 54, NULL, 6, 0, '0'),
(1291, NULL, 54, NULL, 7, 0, '0'),
(1292, NULL, 54, NULL, 8, 0, '0'),
(1293, NULL, 54, NULL, 10, 0, '0'),
(1294, NULL, 54, NULL, 11, 0, '0'),
(1295, NULL, 54, NULL, 2, 0, '0'),
(1296, NULL, 54, NULL, 18, 0, '0'),
(1297, NULL, 54, NULL, 19, 0, '0'),
(1298, NULL, 54, NULL, 20, 0, '0'),
(1299, NULL, 54, NULL, 17, 0, '0'),
(1300, NULL, 54, NULL, 16, 0, '0'),
(1301, NULL, 54, NULL, 14, 0, '0'),
(1302, NULL, 54, NULL, 13, 0, '0'),
(1303, NULL, 54, NULL, 12, 0, '0'),
(1304, NULL, 54, NULL, 9, 0, '0'),
(1305, NULL, 54, NULL, 21, 0, '0'),
(1306, NULL, 55, NULL, 4, 0, '0'),
(1307, NULL, 55, NULL, 5, 0, '0'),
(1308, NULL, 55, NULL, 6, 0, '0'),
(1309, NULL, 55, NULL, 7, 0, '0'),
(1310, NULL, 55, NULL, 8, 0, '0'),
(1311, NULL, 55, NULL, 10, 0, '0'),
(1312, NULL, 55, NULL, 11, 0, '0'),
(1313, NULL, 55, NULL, 2, 0, '0'),
(1314, NULL, 55, NULL, 18, 0, '0'),
(1315, NULL, 55, NULL, 19, 0, '0'),
(1316, NULL, 55, NULL, 20, 0, '0'),
(1317, NULL, 55, NULL, 17, 0, '0'),
(1318, NULL, 55, NULL, 16, 0, '0'),
(1319, NULL, 55, NULL, 14, 0, '0'),
(1320, NULL, 55, NULL, 13, 0, '0'),
(1321, NULL, 55, NULL, 12, 0, '0'),
(1322, NULL, 55, NULL, 9, 0, '0'),
(1323, NULL, 55, NULL, 21, 0, '0'),
(1324, NULL, 56, NULL, 4, 0, '0'),
(1325, NULL, 56, NULL, 5, 0, '0'),
(1326, NULL, 56, NULL, 6, 0, '0'),
(1327, NULL, 56, NULL, 7, 0, '0'),
(1328, NULL, 56, NULL, 8, 0, '0'),
(1329, NULL, 56, NULL, 10, 0, '0'),
(1330, NULL, 56, NULL, 11, 0, '0'),
(1331, NULL, 56, NULL, 2, 0, '0'),
(1332, NULL, 56, NULL, 18, 0, '0'),
(1333, NULL, 56, NULL, 19, 0, '0'),
(1334, NULL, 56, NULL, 20, 0, '0'),
(1335, NULL, 56, NULL, 17, 0, '0'),
(1336, NULL, 56, NULL, 16, 0, '0'),
(1337, NULL, 56, NULL, 14, 0, '0'),
(1338, NULL, 56, NULL, 13, 0, '0'),
(1339, NULL, 56, NULL, 12, 0, '0'),
(1340, NULL, 56, NULL, 9, 0, '0'),
(1341, NULL, 56, NULL, 21, 0, '0'),
(1342, NULL, 57, NULL, 4, 0, '0'),
(1343, NULL, 57, NULL, 5, 0, '0'),
(1344, NULL, 57, NULL, 6, 0, '0'),
(1345, NULL, 57, NULL, 7, 0, '0'),
(1346, NULL, 57, NULL, 8, 0, '0'),
(1347, NULL, 57, NULL, 10, 0, '0'),
(1348, NULL, 57, NULL, 11, 0, '0'),
(1349, NULL, 57, NULL, 2, 0, '0'),
(1350, NULL, 57, NULL, 18, 0, '0'),
(1351, NULL, 57, NULL, 19, 0, '0'),
(1352, NULL, 57, NULL, 20, 0, '0'),
(1353, NULL, 57, NULL, 17, 0, '0'),
(1354, NULL, 57, NULL, 16, 0, '0'),
(1355, NULL, 57, NULL, 14, 0, '0'),
(1356, NULL, 57, NULL, 13, 0, '0'),
(1357, NULL, 57, NULL, 12, 0, '0'),
(1358, NULL, 57, NULL, 9, 0, '0'),
(1359, NULL, 57, NULL, 21, 0, '0'),
(1360, NULL, 58, NULL, 4, 0, '0'),
(1361, NULL, 58, NULL, 5, 0, '0'),
(1362, NULL, 58, NULL, 6, 0, '0'),
(1363, NULL, 58, NULL, 7, 0, '0'),
(1364, NULL, 58, NULL, 8, 0, '0'),
(1365, NULL, 58, NULL, 10, 0, '0'),
(1366, NULL, 58, NULL, 11, 0, '0'),
(1367, NULL, 58, NULL, 2, 0, '0'),
(1368, NULL, 58, NULL, 18, 0, '0'),
(1369, NULL, 58, NULL, 19, 0, '0'),
(1370, NULL, 58, NULL, 20, 0, '0'),
(1371, NULL, 58, NULL, 17, 0, '0'),
(1372, NULL, 58, NULL, 16, 0, '0'),
(1373, NULL, 58, NULL, 14, 0, '0'),
(1374, NULL, 58, NULL, 13, 0, '0'),
(1375, NULL, 58, NULL, 12, 0, '0'),
(1376, NULL, 58, NULL, 9, 0, '0'),
(1377, NULL, 58, NULL, 21, 0, '0'),
(1378, NULL, 59, NULL, 4, 0, '0'),
(1379, NULL, 59, NULL, 5, 0, '0'),
(1380, NULL, 59, NULL, 6, 0, '0'),
(1381, NULL, 59, NULL, 7, 0, '0'),
(1382, NULL, 59, NULL, 8, 0, '0'),
(1383, NULL, 59, NULL, 10, 0, '0'),
(1384, NULL, 59, NULL, 11, 0, '0'),
(1385, NULL, 59, NULL, 2, 0, '0'),
(1386, NULL, 59, NULL, 18, 0, '0'),
(1387, NULL, 59, NULL, 19, 0, '0'),
(1388, NULL, 59, NULL, 20, 0, '0'),
(1389, NULL, 59, NULL, 17, 0, '0'),
(1390, NULL, 59, NULL, 16, 0, '0'),
(1391, NULL, 59, NULL, 14, 0, '0'),
(1392, NULL, 59, NULL, 13, 0, '0'),
(1393, NULL, 59, NULL, 12, 0, '0'),
(1394, NULL, 59, NULL, 9, 0, '0'),
(1395, NULL, 59, NULL, 21, 0, '0'),
(1396, NULL, 60, NULL, 4, 0, '0'),
(1397, NULL, 60, NULL, 5, 0, '0'),
(1398, NULL, 60, NULL, 6, 0, '0'),
(1399, NULL, 60, NULL, 7, 0, '0'),
(1400, NULL, 60, NULL, 8, 0, '0'),
(1401, NULL, 60, NULL, 10, 0, '0'),
(1402, NULL, 60, NULL, 11, 0, '0'),
(1403, NULL, 60, NULL, 2, 0, '0'),
(1404, NULL, 60, NULL, 18, 0, '0'),
(1405, NULL, 60, NULL, 19, 0, '0'),
(1406, NULL, 60, NULL, 20, 0, '0'),
(1407, NULL, 60, NULL, 17, 0, '0'),
(1408, NULL, 60, NULL, 16, 0, '0'),
(1409, NULL, 60, NULL, 14, 0, '0'),
(1410, NULL, 60, NULL, 13, 0, '0'),
(1411, NULL, 60, NULL, 12, 0, '0'),
(1412, NULL, 60, NULL, 9, 0, '0'),
(1413, NULL, 60, NULL, 21, 0, '0'),
(1414, NULL, 61, NULL, 4, 0, '0'),
(1415, NULL, 61, NULL, 5, 0, '0'),
(1416, NULL, 61, NULL, 6, 0, '0'),
(1417, NULL, 61, NULL, 7, 0, '0'),
(1418, NULL, 61, NULL, 8, 0, '0'),
(1419, NULL, 61, NULL, 10, 0, '0'),
(1420, NULL, 61, NULL, 11, 0, '0'),
(1421, NULL, 61, NULL, 2, 0, '0'),
(1422, NULL, 61, NULL, 18, 0, '0'),
(1423, NULL, 61, NULL, 19, 0, '0'),
(1424, NULL, 61, NULL, 20, 0, '0'),
(1425, NULL, 61, NULL, 17, 0, '0'),
(1426, NULL, 61, NULL, 16, 0, '0'),
(1427, NULL, 61, NULL, 14, 0, '0'),
(1428, NULL, 61, NULL, 13, 0, '0'),
(1429, NULL, 61, NULL, 12, 0, '0'),
(1430, NULL, 61, NULL, 9, 0, '0'),
(1431, NULL, 61, NULL, 21, 0, '0'),
(1432, NULL, 62, NULL, 4, 0, '0'),
(1433, NULL, 62, NULL, 5, 0, '0'),
(1434, NULL, 62, NULL, 6, 0, '0'),
(1435, NULL, 62, NULL, 7, 0, '0'),
(1436, NULL, 62, NULL, 8, 0, '0'),
(1437, NULL, 62, NULL, 10, 0, '0'),
(1438, NULL, 62, NULL, 11, 0, '0'),
(1439, NULL, 62, NULL, 2, 0, '0'),
(1440, NULL, 62, NULL, 18, 0, '0'),
(1441, NULL, 62, NULL, 19, 0, '0'),
(1442, NULL, 62, NULL, 20, 0, '0'),
(1443, NULL, 62, NULL, 17, 0, '0'),
(1444, NULL, 62, NULL, 16, 0, '0'),
(1445, NULL, 62, NULL, 14, 0, '0'),
(1446, NULL, 62, NULL, 13, 0, '0'),
(1447, NULL, 62, NULL, 12, 0, '0'),
(1448, NULL, 62, NULL, 9, 0, '0'),
(1449, NULL, 62, NULL, 21, 0, '0'),
(1450, NULL, 63, NULL, 4, 0, '0'),
(1451, NULL, 63, NULL, 5, 0, '0'),
(1452, NULL, 63, NULL, 6, 0, '0'),
(1453, NULL, 63, NULL, 7, 0, '0'),
(1454, NULL, 63, NULL, 8, 0, '0'),
(1455, NULL, 63, NULL, 10, 0, '0'),
(1456, NULL, 63, NULL, 11, 0, '0'),
(1457, NULL, 63, NULL, 2, 0, '0'),
(1458, NULL, 63, NULL, 18, 0, '0'),
(1459, NULL, 63, NULL, 19, 0, '0'),
(1460, NULL, 63, NULL, 20, 0, '0'),
(1461, NULL, 63, NULL, 17, 0, '0'),
(1462, NULL, 63, NULL, 16, 0, '0'),
(1463, NULL, 63, NULL, 14, 0, '0'),
(1464, NULL, 63, NULL, 13, 0, '0'),
(1465, NULL, 63, NULL, 12, 0, '0'),
(1466, NULL, 63, NULL, 9, 0, '0'),
(1467, NULL, 63, NULL, 21, 0, '0'),
(1468, NULL, 64, NULL, 4, 0, '0'),
(1469, NULL, 64, NULL, 5, 0, '0'),
(1470, NULL, 64, NULL, 6, 0, '0'),
(1471, NULL, 64, NULL, 7, 0, '0'),
(1472, NULL, 64, NULL, 8, 0, '0'),
(1473, NULL, 64, NULL, 10, 0, '0'),
(1474, NULL, 64, NULL, 11, 0, '0'),
(1475, NULL, 64, NULL, 2, 0, '0'),
(1476, NULL, 64, NULL, 18, 0, '0'),
(1477, NULL, 64, NULL, 19, 0, '0'),
(1478, NULL, 64, NULL, 20, 0, '0'),
(1479, NULL, 64, NULL, 17, 0, '0'),
(1480, NULL, 64, NULL, 16, 0, '0'),
(1481, NULL, 64, NULL, 14, 0, '0'),
(1482, NULL, 64, NULL, 13, 0, '0'),
(1483, NULL, 64, NULL, 12, 0, '0'),
(1484, NULL, 64, NULL, 9, 0, '0'),
(1485, NULL, 64, NULL, 21, 0, '0'),
(1486, NULL, 65, NULL, 4, 0, '0'),
(1487, NULL, 65, NULL, 5, 0, '0'),
(1488, NULL, 65, NULL, 6, 0, '0'),
(1489, NULL, 65, NULL, 7, 0, '0'),
(1490, NULL, 65, NULL, 8, 0, '0'),
(1491, NULL, 65, NULL, 10, 0, '0'),
(1492, NULL, 65, NULL, 11, 0, '0'),
(1493, NULL, 65, NULL, 2, 0, '0'),
(1494, NULL, 65, NULL, 18, 0, '0'),
(1495, NULL, 65, NULL, 19, 0, '0'),
(1496, NULL, 65, NULL, 20, 0, '0'),
(1497, NULL, 65, NULL, 17, 0, '0'),
(1498, NULL, 65, NULL, 16, 0, '0'),
(1499, NULL, 65, NULL, 14, 0, '0'),
(1500, NULL, 65, NULL, 13, 0, '0'),
(1501, NULL, 65, NULL, 12, 0, '0'),
(1502, NULL, 65, NULL, 9, 0, '0'),
(1503, NULL, 65, NULL, 21, 0, '0'),
(1504, NULL, 73, NULL, 4, 0, '0'),
(1505, NULL, 73, NULL, 5, 6, '1050000'),
(1506, NULL, 73, NULL, 6, 0, '0'),
(1507, NULL, 73, NULL, 7, 0, '0'),
(1508, NULL, 73, NULL, 8, 1, '100000'),
(1509, NULL, 73, NULL, 10, 0, '0'),
(1510, NULL, 73, NULL, 11, 0, '0'),
(1511, NULL, 73, NULL, 2, 0, '0'),
(1512, NULL, 73, NULL, 18, 0, '0'),
(1513, NULL, 73, NULL, 19, 0, '0'),
(1514, NULL, 73, NULL, 20, 0, '0'),
(1515, NULL, 73, NULL, 17, 0, '0'),
(1516, NULL, 73, NULL, 16, 0, '0'),
(1517, NULL, 73, NULL, 14, 0, '0'),
(1518, NULL, 73, NULL, 13, 0, '0'),
(1519, NULL, 73, NULL, 12, 0, '0'),
(1520, NULL, 73, NULL, 9, 0, '0'),
(1521, NULL, 73, NULL, 21, 0, '0'),
(1522, NULL, 74, NULL, 4, 0, '0'),
(1523, NULL, 74, NULL, 5, 12, '2100000'),
(1524, NULL, 74, NULL, 6, 0, '0'),
(1525, NULL, 74, NULL, 7, 0, '0'),
(1526, NULL, 74, NULL, 8, 1, '100000'),
(1527, NULL, 74, NULL, 10, 0, '0'),
(1528, NULL, 74, NULL, 11, 0, '0'),
(1529, NULL, 74, NULL, 2, 0, '0'),
(1530, NULL, 74, NULL, 18, 0, '0'),
(1531, NULL, 74, NULL, 19, 0, '0'),
(1532, NULL, 74, NULL, 20, 0, '0'),
(1533, NULL, 74, NULL, 17, 0, '0'),
(1534, NULL, 74, NULL, 16, 0, '0'),
(1535, NULL, 74, NULL, 14, 0, '0'),
(1536, NULL, 74, NULL, 13, 0, '0'),
(1537, NULL, 74, NULL, 12, 0, '0'),
(1538, NULL, 74, NULL, 9, 0, '0'),
(1539, NULL, 74, NULL, 21, 0, '0'),
(1540, NULL, 75, NULL, 4, 0, '0'),
(1541, NULL, 75, NULL, 5, 5, '875000'),
(1542, NULL, 75, NULL, 6, 0, '0'),
(1543, NULL, 75, NULL, 7, 0, '0'),
(1544, NULL, 75, NULL, 8, 0, '0'),
(1545, NULL, 75, NULL, 10, 0, '0'),
(1546, NULL, 75, NULL, 11, 0, '0'),
(1547, NULL, 75, NULL, 2, 0, '0'),
(1548, NULL, 75, NULL, 18, 0, '0'),
(1549, NULL, 75, NULL, 19, 0, '0'),
(1550, NULL, 75, NULL, 20, 0, '0'),
(1551, NULL, 75, NULL, 17, 0, '0'),
(1552, NULL, 75, NULL, 16, 0, '0'),
(1553, NULL, 75, NULL, 14, 0, '0'),
(1554, NULL, 75, NULL, 13, 0, '0'),
(1555, NULL, 75, NULL, 12, 0, '0'),
(1556, NULL, 75, NULL, 9, 0, '0'),
(1557, NULL, 75, NULL, 21, 0, '0'),
(1558, NULL, 76, NULL, 4, 0, '0'),
(1559, NULL, 76, NULL, 5, 5, '875000'),
(1560, NULL, 76, NULL, 6, 0, '0'),
(1561, NULL, 76, NULL, 7, 0, '0'),
(1562, NULL, 76, NULL, 8, 0, '0'),
(1563, NULL, 76, NULL, 10, 0, '0'),
(1564, NULL, 76, NULL, 11, 0, '0'),
(1565, NULL, 76, NULL, 2, 0, '0'),
(1566, NULL, 76, NULL, 18, 0, '0'),
(1567, NULL, 76, NULL, 19, 0, '0'),
(1568, NULL, 76, NULL, 20, 0, '0'),
(1569, NULL, 76, NULL, 17, 0, '0'),
(1570, NULL, 76, NULL, 16, 0, '0'),
(1571, NULL, 76, NULL, 14, 0, '0'),
(1572, NULL, 76, NULL, 13, 0, '0'),
(1573, NULL, 76, NULL, 12, 0, '0'),
(1574, NULL, 76, NULL, 9, 0, '0'),
(1575, NULL, 76, NULL, 21, 0, '0'),
(1576, NULL, 77, NULL, 4, 0, '0'),
(1577, NULL, 77, NULL, 5, 5, '875000'),
(1578, NULL, 77, NULL, 6, 0, '0'),
(1579, NULL, 77, NULL, 7, 0, '0'),
(1580, NULL, 77, NULL, 8, 0, '0'),
(1581, NULL, 77, NULL, 10, 0, '0'),
(1582, NULL, 77, NULL, 11, 0, '0'),
(1583, NULL, 77, NULL, 2, 0, '0'),
(1584, NULL, 77, NULL, 18, 0, '0'),
(1585, NULL, 77, NULL, 19, 0, '0'),
(1586, NULL, 77, NULL, 20, 0, '0'),
(1587, NULL, 77, NULL, 17, 0, '0'),
(1588, NULL, 77, NULL, 16, 0, '0'),
(1589, NULL, 77, NULL, 14, 0, '0'),
(1590, NULL, 77, NULL, 13, 0, '0'),
(1591, NULL, 77, NULL, 12, 0, '0'),
(1592, NULL, 77, NULL, 9, 0, '0'),
(1593, NULL, 77, NULL, 21, 0, '0'),
(1594, NULL, 78, NULL, 4, 0, '0'),
(1595, NULL, 78, NULL, 5, 12, '2100000'),
(1596, NULL, 78, NULL, 6, 0, '0'),
(1597, NULL, 78, NULL, 7, 0, '0'),
(1598, NULL, 78, NULL, 8, 0, '0'),
(1599, NULL, 78, NULL, 10, 0, '0'),
(1600, NULL, 78, NULL, 11, 0, '0'),
(1601, NULL, 78, NULL, 2, 0, '0'),
(1602, NULL, 78, NULL, 18, 0, '0'),
(1603, NULL, 78, NULL, 19, 0, '0'),
(1604, NULL, 78, NULL, 20, 0, '0'),
(1605, NULL, 78, NULL, 17, 0, '0'),
(1606, NULL, 78, NULL, 16, 0, '0'),
(1607, NULL, 78, NULL, 14, 0, '0'),
(1608, NULL, 78, NULL, 13, 0, '0'),
(1609, NULL, 78, NULL, 12, 0, '0'),
(1610, NULL, 78, NULL, 9, 0, '0'),
(1611, NULL, 78, NULL, 21, 0, '0'),
(1612, NULL, 79, NULL, 4, 2, '350000'),
(1613, NULL, 79, NULL, 5, 0, '0'),
(1614, NULL, 79, NULL, 6, 0, '0'),
(1615, NULL, 79, NULL, 7, 0, '0'),
(1616, NULL, 79, NULL, 8, 0, '0'),
(1617, NULL, 79, NULL, 10, 0, '0'),
(1618, NULL, 79, NULL, 11, 0, '0'),
(1619, NULL, 79, NULL, 2, 0, '0'),
(1620, NULL, 79, NULL, 18, 0, '0'),
(1621, NULL, 79, NULL, 19, 0, '0'),
(1622, NULL, 79, NULL, 20, 0, '0'),
(1623, NULL, 79, NULL, 17, 0, '0'),
(1624, NULL, 79, NULL, 16, 0, '0'),
(1625, NULL, 79, NULL, 14, 0, '0'),
(1626, NULL, 79, NULL, 13, 0, '0'),
(1627, NULL, 79, NULL, 12, 0, '0'),
(1628, NULL, 79, NULL, 9, 0, '0'),
(1629, NULL, 79, NULL, 21, 0, '0'),
(1630, NULL, 80, NULL, 4, 2, '350000'),
(1631, NULL, 80, NULL, 5, 0, '0'),
(1632, NULL, 80, NULL, 6, 0, '0'),
(1633, NULL, 80, NULL, 7, 0, '0'),
(1634, NULL, 80, NULL, 8, 0, '0'),
(1635, NULL, 80, NULL, 10, 0, '0'),
(1636, NULL, 80, NULL, 11, 0, '0'),
(1637, NULL, 80, NULL, 2, 0, '0'),
(1638, NULL, 80, NULL, 18, 0, '0'),
(1639, NULL, 80, NULL, 19, 0, '0'),
(1640, NULL, 80, NULL, 20, 0, '0'),
(1641, NULL, 80, NULL, 17, 0, '0'),
(1642, NULL, 80, NULL, 16, 0, '0'),
(1643, NULL, 80, NULL, 14, 0, '0'),
(1644, NULL, 80, NULL, 13, 0, '0'),
(1645, NULL, 80, NULL, 12, 0, '0'),
(1646, NULL, 80, NULL, 9, 0, '0'),
(1647, NULL, 80, NULL, 21, 0, '0'),
(1648, NULL, 81, NULL, 4, 2, '350000'),
(1649, NULL, 81, NULL, 5, 0, '0'),
(1650, NULL, 81, NULL, 6, 0, '0'),
(1651, NULL, 81, NULL, 7, 0, '0'),
(1652, NULL, 81, NULL, 8, 0, '0'),
(1653, NULL, 81, NULL, 10, 0, '0'),
(1654, NULL, 81, NULL, 11, 0, '0'),
(1655, NULL, 81, NULL, 2, 0, '0'),
(1656, NULL, 81, NULL, 18, 0, '0'),
(1657, NULL, 81, NULL, 19, 0, '0'),
(1658, NULL, 81, NULL, 20, 0, '0'),
(1659, NULL, 81, NULL, 17, 0, '0'),
(1660, NULL, 81, NULL, 16, 0, '0'),
(1661, NULL, 81, NULL, 14, 0, '0'),
(1662, NULL, 81, NULL, 13, 0, '0'),
(1663, NULL, 81, NULL, 12, 0, '0'),
(1664, NULL, 81, NULL, 9, 0, '0'),
(1665, NULL, 81, NULL, 21, 0, '0'),
(1667, 1, 101, NULL, 5, 4, '700000'),
(1681, 2, 101, NULL, 12, 0, '300000'),
(1684, NULL, 102, NULL, 4, 0, '0'),
(1685, NULL, 102, NULL, 5, 4, '700000'),
(1686, NULL, 102, NULL, 6, 0, '0'),
(1687, NULL, 102, NULL, 7, 0, '0'),
(1688, NULL, 102, NULL, 8, 0, '0'),
(1689, NULL, 102, NULL, 10, 0, '0'),
(1690, NULL, 102, NULL, 11, 0, '0'),
(1691, NULL, 102, NULL, 2, 0, '0'),
(1692, NULL, 102, NULL, 18, 0, '0'),
(1693, NULL, 102, NULL, 19, 0, '0'),
(1694, NULL, 102, NULL, 20, 0, '0'),
(1695, NULL, 102, NULL, 17, 0, '0'),
(1696, NULL, 102, NULL, 16, 0, '0'),
(1697, NULL, 102, NULL, 14, 0, '0'),
(1698, NULL, 102, NULL, 13, 0, '0'),
(1699, NULL, 102, NULL, 12, 0, '300000'),
(1700, NULL, 102, NULL, 9, 0, '0'),
(1701, NULL, 102, NULL, 21, 0, '0'),
(1720, NULL, 156, NULL, 4, 2, '350000'),
(1721, NULL, 156, NULL, 5, 0, '0'),
(1722, NULL, 156, NULL, 6, 1, '90000'),
(1723, NULL, 156, NULL, 7, 0, '0'),
(1724, NULL, 156, NULL, 8, 0, '0'),
(1725, NULL, 156, NULL, 10, 0, '0'),
(1726, NULL, 156, NULL, 11, 0, '0'),
(1727, NULL, 156, NULL, 2, 0, '0'),
(1728, NULL, 156, NULL, 18, 0, '0'),
(1729, NULL, 156, NULL, 19, 0, '0'),
(1730, NULL, 156, NULL, 20, 0, '0'),
(1731, NULL, 156, NULL, 17, 0, '40000'),
(1732, NULL, 156, NULL, 16, 0, '0'),
(1733, NULL, 156, NULL, 14, 0, '0'),
(1734, NULL, 156, NULL, 13, 0, '0'),
(1735, NULL, 156, NULL, 12, 0, '0'),
(1736, NULL, 156, NULL, 9, 0, '0'),
(1737, NULL, 156, NULL, 21, 0, '0'),
(1774, NULL, NULL, 4, 4, 1, '175000'),
(1775, NULL, NULL, 4, 5, 1, '175000'),
(1776, NULL, NULL, 4, 6, 0, '0'),
(1777, NULL, NULL, 4, 7, 0, '0'),
(1778, NULL, NULL, 4, 8, 0, '0'),
(1779, NULL, NULL, 4, 10, 0, '0'),
(1780, NULL, NULL, 4, 11, 0, '0'),
(1781, NULL, NULL, 4, 2, 0, '0'),
(1782, NULL, NULL, 4, 18, 0, '0'),
(1783, NULL, NULL, 4, 19, 0, '0'),
(1784, NULL, NULL, 4, 20, 0, '0'),
(1785, NULL, NULL, 4, 17, 0, '0'),
(1786, NULL, NULL, 4, 16, 0, '0'),
(1787, NULL, NULL, 4, 14, 0, '0'),
(1788, NULL, NULL, 4, 13, 0, '0'),
(1789, NULL, NULL, 4, 12, 0, '0'),
(1790, NULL, NULL, 4, 9, 0, '0'),
(1791, NULL, NULL, 4, 21, 0, '0'),
(1792, 1, 103, NULL, 6, 1, '90000'),
(1793, 2, 103, NULL, 7, 1, '90000'),
(1794, 3, 103, NULL, 8, 1, '100000'),
(1795, 1, 162, NULL, 6, 1, NULL),
(1796, 2, 162, NULL, 7, 1, NULL),
(1797, 3, 162, NULL, 8, 1, NULL),
(1798, NULL, 103, NULL, 4, 0, '0'),
(1799, NULL, 103, NULL, 5, 0, '0'),
(1800, NULL, 103, NULL, 10, 0, '0'),
(1801, NULL, 103, NULL, 11, 0, '0'),
(1802, NULL, 103, NULL, 2, 0, '0'),
(1803, NULL, 103, NULL, 18, 0, '0'),
(1804, NULL, 103, NULL, 19, 0, '0'),
(1805, NULL, 103, NULL, 20, 0, '0'),
(1806, NULL, 103, NULL, 17, 0, '0'),
(1807, NULL, 103, NULL, 16, 0, '0'),
(1808, NULL, 103, NULL, 14, 0, '0'),
(1809, NULL, 103, NULL, 13, 0, '0'),
(1810, NULL, 103, NULL, 12, 0, '0'),
(1811, NULL, 103, NULL, 9, 0, '0'),
(1812, NULL, 103, NULL, 21, 0, '0'),
(1831, NULL, 166, NULL, 6, 1, '90000'),
(1832, NULL, 166, NULL, 7, 1, '90000'),
(1833, NULL, 166, NULL, 8, 1, '100000');

-- --------------------------------------------------------

--
-- Table structure for table `flow`
--

CREATE TABLE `flow` (
  `id` int(11) NOT NULL,
  `estimasi` int(11) NOT NULL,
  `scan_in` datetime DEFAULT NULL,
  `scan_out` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flow`
--

INSERT INTO `flow` (`id`, `estimasi`, `scan_in`, `scan_out`) VALUES
(14, 1792, '2020-06-20 14:53:09', NULL),
(15, 1792, '2020-06-20 19:54:06', NULL),
(16, 1792, NULL, '2020-06-20 20:08:12'),
(17, 1792, '2020-06-20 20:13:51', NULL),
(18, 1792, NULL, '2020-07-01 02:13:07'),
(19, 1792, NULL, '2020-07-01 02:13:13'),
(20, 1793, '2020-07-01 02:14:06', NULL),
(21, 1793, NULL, '2020-07-01 02:14:13'),
(22, 1793, '2020-07-01 02:14:24', NULL),
(23, 1793, NULL, '2020-07-01 02:14:30'),
(25, 1793, '2020-07-01 02:15:02', NULL),
(26, 1793, NULL, '2020-07-01 02:15:08'),
(27, 1794, '2020-07-01 02:15:20', NULL),
(28, 1794, NULL, '2020-07-01 02:15:32'),
(29, 1794, '2020-07-01 02:15:44', NULL),
(30, 1794, NULL, '2020-07-01 02:15:59'),
(33, 1794, '2020-07-01 02:54:57', NULL),
(38, 1794, NULL, '2020-07-01 02:59:33'),
(40, 1831, '2020-07-08 03:15:55', NULL),
(41, 1831, NULL, '2020-07-08 03:16:35'),
(42, 1832, '2020-07-08 03:16:59', NULL),
(43, 1832, NULL, '2020-07-08 03:17:09'),
(44, 1833, '2020-07-08 03:17:18', NULL),
(45, 1833, NULL, '2020-07-08 03:17:31');

-- --------------------------------------------------------

--
-- Table structure for table `list_material`
--

CREATE TABLE `list_material` (
  `id` int(11) NOT NULL,
  `material` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `panjang` varchar(10) DEFAULT NULL,
  `lebar` varchar(10) DEFAULT NULL,
  `tinggi` varchar(10) DEFAULT NULL,
  `weightplate` varchar(100) DEFAULT NULL,
  `weightround` varchar(100) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `status` varchar(40) NOT NULL,
  `aktualharga` int(11) DEFAULT NULL,
  `supplier` varchar(100) DEFAULT NULL,
  `part` int(11) DEFAULT NULL,
  `no_gambar` varchar(30) DEFAULT NULL,
  `tipe` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `list_material`
--

INSERT INTO `list_material` (`id`, `material`, `qty`, `panjang`, `lebar`, `tinggi`, `weightplate`, `weightround`, `harga`, `status`, `aktualharga`, `supplier`, `part`, `no_gambar`, `tipe`) VALUES
(34, 26, 3, '', '55', '590', '', '5604115.000000001', 195000, 'Terkirim ke Supplier', 1352000, 'COY STEEL', 51, '01', 'proses'),
(35, 21, 1, '505', '155', '15', '1174125', '', 500417, 'Terkirim ke Supplier', 655000, 'GANESHA', 73, '', 'proses'),
(36, 21, 1, '400', '155', '30', '1860000', '', 602640, 'Terkirim ke Supplier', 975000, 'GANESHA', 74, '', 'proses'),
(37, 21, 1, '400', '155', '15', '930000', '', 301320, 'Terkirim ke Supplier', 520000, 'GANESHA', 75, '', 'proses'),
(38, 21, 1, '505', '155', '15', '1174125', '', 380417, 'Terkirim ke Supplier', 655000, 'GANESHA', 76, '', 'proses'),
(39, 21, 1, '355', '95', '12', '404700', '', 131123, 'Terkirim ke Supplier', 220000, 'GANESHA', 77, '', 'proses'),
(40, 21, 1, '400', '155', '25', '1550000', '', 502200, 'Terkirim ke Supplier', 810000, 'GANESHA', 78, '', 'proses'),
(41, 27, 1, '', '16', '200', '', '160768', 308675, 'Terkirim ke Supplier', 130000, 'GANESHA', 79, '', 'proses'),
(42, 16, 1, '', '10', '1000', '0', '314000', 208182, 'Terkirim ke Supplier', 192000, 'COY STEEL', 80, '', 'proses'),
(43, 16, 1, '', '12', '500', '', '226079.99999999997', 149891, 'Terkirim ke Supplier', 120000, 'COY STEEL', 81, '', 'proses'),
(44, 15, 3, '', '95', '70', '', '1983695', 696277, 'Terkirim ke Supplier', 189000, 'MANDIRI STEEL', 52, '02', 'proses'),
(45, 15, 1, '150', '360', '15', '810000', '', 94770, 'Terkirim ke Supplier', 86000, 'MANDIRI STEEL', 53, '03 A', 'proses'),
(46, 15, 1, '', '55', '320', '', '3039520.0000000005', 355624, 'Terkirim ke Supplier', 117000, 'MANDIRI STEEL', 54, '03 B', 'proses'),
(47, 15, 1, '215', '130', '15', '419250', '', 49052, 'Terkirim ke Supplier', 45000, 'MANDIRI STEEL', 55, '03 C', 'proses'),
(48, 15, 1, '', '45', '100', '', '635850.0000000001', 74394, 'Terkirim ke Supplier', 25000, 'MANDIRI STEEL', 56, '04', 'proses'),
(49, 15, 1, '', '45', '50', '', '317925.00000000006', 37197, 'Terkirim ke Supplier', 12000, 'MANDIRI STEEL', 57, '05', 'proses'),
(50, 15, 1, '', '32', '500', '', '1607680', 188099, 'Terkirim ke Supplier', 61500, 'MANDIRI STEEL', 58, '06', 'proses'),
(51, 15, 3, '', '36', '55', '', '223819.2', 78561, 'Terkirim ke Supplier', 30000, 'MANDIRI STEEL', 59, '07', 'proses'),
(52, 28, 1, '', '55', '180', '', '1709730.0000000002', 85487, 'Terkirim ke Supplier', 60000, 'GANESHA', 60, '08', 'proses'),
(53, 15, 3, '105', '55', '45', '259875', '', 91216, 'Terkirim ke Supplier', 90000, 'MANDIRI STEEL', 61, '09', 'proses'),
(54, 15, 1, '425', '140', '15', '892500', '', 104422, 'Terkirim ke Supplier', 95000, 'MANDIRI STEEL', 62, '10', 'proses'),
(55, 16, 1, '', '22', '500', '', '759880', 503800, 'Terkirim ke Supplier', 180000, 'COY STEEL', 63, '11', 'proses'),
(56, 15, 3, '', '55', '30', '', '284955.00000000006', 100019, 'Terkirim ke Supplier', 27000, 'MANDIRI STEEL', 64, '12', 'proses'),
(57, 16, 3, '140', '110', '50', '770000', '', 1531530, 'Terkirim ke Supplier', 1683000, 'COY STEEL', 65, '13', 'proses'),
(58, 18, 2, '45', '20', '20', '18000', '0', 21060, 'Terkirim ke Supplier', 140000, 'COY STEEL', 66, '01', 'proses'),
(59, 18, 2, '130', '85', '40', '442000', '', 517140, 'Terkirim ke Supplier', 553000, 'COY STEEL', 67, '02', 'proses'),
(60, 18, 1, '', '12', '500', '', '226079.99999999997', 132257, 'Terkirim ke Supplier', 80000, 'COY STEEL', 68, '03', 'proses'),
(61, 18, 1, '', '12', '50', '', '22608', 13226, 'Terkirim ke Supplier', 1, 'COY STEEL', 69, '04', 'proses'),
(62, 14, 1, '60', '80', '15', '72000', '', 12355, 'Terkirim ke Supplier', 22000, 'COY STEEL', 70, '05', 'proses'),
(63, 18, 2, '60', '35', '20', '42000', '', 49140, 'Terkirim ke Supplier', 140000, 'COY STEEL', 71, '06', 'proses'),
(64, 14, 2, '100', '40', '0.5', '2000', '', 686, 'Terkirim ke Supplier', 1, 'COY STEEL', 72, '07', 'proses'),
(65, 18, 2, '120', '80', '35', '336000', '', 393120, 'Terkirim ke Supplier', 413000, 'COY STEEL', 82, '01', 'proses'),
(66, 14, 2, '120', '115', '20', '276000', '', 94723, 'Terkirim ke Supplier', 101200, 'COY STEEL', 83, '02', 'proses'),
(67, 14, 2, '90', '85', '25', '191250', '', 65637, 'Terkirim ke Supplier', 72600, 'COY STEEL', 84, '03', 'proses'),
(68, 18, 2, '45', '55', '35', '86625', '', 101351, 'Terkirim ke Supplier', 140000, 'COY STEEL', 85, '04', 'proses'),
(69, 18, 2, '', '50', '40', '', '314000', 367380, 'Terkirim ke Supplier', 140000, 'COY STEEL', 86, '05', 'proses'),
(70, 19, 1, '', '10', '500', '', '157000', 55107, 'Terkirim ke Supplier', 96000, 'COY STEEL', 87, '06', 'proses'),
(71, 20, 2, '200', '15', '0.6', '1800', '', 3456, 'Terkirim ke Supplier', 1, 'GANESHS', 88, '07', 'proses'),
(72, 15, 2, '32', '17', '20', '10880', '', 2546, 'Terkirim ke Supplier', 4000, 'MANDIRI STEEL', 89, '08', 'proses'),
(73, 20, 1, '', '275', '30', '', '7123875', 6838920, 'Terkirim ke Supplier', 1830000, 'GANESHA', 90, '01', 'proses'),
(74, 18, 1, '', '150', '60', '', '4239000', 2479815, 'Terkirim ke Supplier', 712000, 'COY STEEL', 91, '02', 'proses'),
(75, 30, 1, '40', '130', '30', '156000', '', 66924, 'Terkirim ke Supplier', 57000, 'COY STEEL', 92, '03', 'proses'),
(76, 14, 1, '17', '10', '13', '2210', '', 379, 'Terkirim ke Supplier', 22000, 'COY STEEL', 93, '04', 'proses'),
(77, 18, 1, '', '32', '100', '', '321536', 188099, 'Terkirim ke Supplier', 70000, 'COY STEEL', 94, '05', 'proses'),
(78, 18, 1, '30', '180', '20', '108000', '', 63180, 'Terkirim ke Supplier', 77000, 'COY STEEL', 95, '06', 'proses'),
(79, 31, 1, '', '75', '50', '', '883125', 132469, 'Terkirim ke Supplier', 55000, 'GANESHA', 96, '07', 'proses'),
(80, 31, 1, '150', '150', '3', '67500', '0', 10125, 'Terkirim ke Supplier', 375000, 'GANESHA', 97, '08', 'proses'),
(81, 31, 1, '', '20', '200', '', '251200', 37680, 'Terkirim ke Supplier', 25000, 'GANESHA', 98, '09', 'proses'),
(82, 17, 10, '120', '75', '55', '495000', '', 1100385, 'Terkirim ke Supplier', 3762000, 'GSM', 99, '', 'proses'),
(83, 17, 5, '128', '75', '50', '480000', '', 1778400, 'Terkirim ke Supplier', 1824000, 'GSM', 100, '', 'proses'),
(84, 17, 3, '177', '105', '32', '594720', '', 1322063, 'Diterima', 1358502, 'TMMIN', 101, '01', 'proses'),
(85, 17, 3, '75', '55', '42', '173250', '', 385135, 'Diterima', 399000, 'GSM', 102, '02', 'proses'),
(86, 17, 3, '200', '105', '32', '672000', '', 1493856, 'Diterima', 1539000, 'GSM', 103, '', 'proses'),
(87, 17, 1, '175', '160', '80', '2240000', '', 1659840, 'Draft', NULL, NULL, 109, '01', 'proses'),
(88, 17, 1, '85', '82', '50', '348500', '', 258238, 'Draft', NULL, NULL, 110, '02', 'proses'),
(89, 16, 1, '', '18', '1000', '', '1017360', 674510, 'Draft', NULL, NULL, 130, '03', 'proses'),
(90, 16, 1, '', '18', '10', '0', '10173.6', 6745, 'Draft', NULL, NULL, 129, '04', 'proses'),
(91, 14, 1, '185', '140', '25', '647500', '', 111111, 'Draft', NULL, NULL, 111, '05', 'proses'),
(92, 14, 1, '', '1', '1', '', '3.14', 1, 'Draft', NULL, NULL, 112, '06', 'proses'),
(93, 14, 1, '68', '50', '12', '40800', '', 7001, 'Draft', NULL, NULL, 113, '07', 'proses'),
(95, 14, 1, '', '85', '85', '', '1928352.5000000002', 330905, 'Draft', NULL, NULL, 156, '01', 'proses'),
(98, 14, 3, '', '8', '7.98', '', '12508.554240000001', 66000, 'Diterima', 70000, NULL, 166, NULL, 'proses');

-- --------------------------------------------------------

--
-- Table structure for table `list_material_fabrikasi`
--

CREATE TABLE `list_material_fabrikasi` (
  `id` int(11) NOT NULL,
  `material` varchar(200) NOT NULL,
  `proyek` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `panjang` varchar(100) NOT NULL,
  `lebar` varchar(100) NOT NULL,
  `tinggi` varchar(100) NOT NULL,
  `harga` varchar(100) NOT NULL,
  `aktualharga` varchar(100) DEFAULT NULL,
  `supplier` varchar(100) DEFAULT NULL,
  `no_gambar` varchar(30) DEFAULT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `list_material_fabrikasi`
--

INSERT INTO `list_material_fabrikasi` (`id`, `material`, `proyek`, `qty`, `panjang`, `lebar`, `tinggi`, `harga`, `aktualharga`, `supplier`, `no_gambar`, `status`) VALUES
(4, 'SIKU', 55, 1, '40', '40', '6000', '100000', '110000', 'ADM', '', 'Diterima'),
(6, 'Siku', 55, 5, '90', '90', '90', '630000', NULL, NULL, 'B-42', 'Terkirim');

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE `material` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `codespek` varchar(40) NOT NULL,
  `beratjenis` varchar(10) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `status` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `material`
--

INSERT INTO `material` (`id`, `nama`, `codespek`, `beratjenis`, `harga`, `status`) VALUES
(14, 'Material', 'S45C', '7.8', 22000, 'Aktif'),
(15, 'Material', 'SS400', '7.8', 15000, 'Aktif'),
(16, 'Material', 'SKD11', '7.8', 85000, 'Aktif'),
(17, 'Material', 'SKD61', '7.8', 95000, 'Aktif'),
(18, 'Material', 'SKS3', '7.8', 75000, 'Aktif'),
(19, 'Material', 'SCM415H', '7.8', 45000, 'Aktif'),
(20, 'Material', 'STAINLES', '8', 120000, 'Aktif'),
(21, 'Material', 'ALUMUNIUM', '2.7', 120000, 'Aktif'),
(22, 'Material', 'CuCr', '9', 450000, 'Aktif'),
(23, 'Material', 'TEMBAGA', '9', 300000, 'Aktif'),
(24, 'Material', 'NAK55', '7.8', 40000, 'Aktif'),
(25, 'Material', 'CuCr Zr', '9', 650000, 'Aktif'),
(26, 'Material', 'VCN50', '8', 65000, 'Aktif'),
(27, 'Material', 'BRONZE', '8', 240000, 'Aktif'),
(28, 'Material', 'NYLON', '1', 50000, 'Aktif'),
(29, 'Material', 'MC BLUE', '1', 60000, 'Aktif'),
(30, 'Material', 'PX4', '7.8', 55000, 'Aktif'),
(31, 'Material', 'BAKELITE', '3', 50000, 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `part`
--

CREATE TABLE `part` (
  `id` int(11) NOT NULL,
  `partname` varchar(100) NOT NULL,
  `qty` int(11) NOT NULL,
  `proyek` int(11) NOT NULL,
  `status` varchar(12) NOT NULL,
  `keterangan` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `part`
--

INSERT INTO `part` (`id`, `partname`, `qty`, `proyek`, `status`, `keterangan`) VALUES
(51, 'EJE BROCA', 3, 36, 'Dibuat', ''),
(52, 'POSICIONADOR', 3, 36, 'Dibuat', ''),
(53, 'PLANCA A', 3, 36, 'Dibuat', ''),
(54, 'PLANCA B', 3, 36, 'Dibuat', ''),
(55, 'PLANCA C', 3, 36, 'Dibuat', ''),
(56, 'ARANDELA', 6, 36, 'Dibuat', ''),
(57, 'ARANDELA 02', 3, 36, 'Dibuat', ''),
(58, 'SEPARADOR', 3, 36, 'Dibuat', ''),
(59, 'TURRION RODAMIENTO', 3, 36, 'Dibuat', ''),
(60, 'SEPARADOR 02', 3, 36, 'Dibuat', ''),
(61, 'COJA RODADURA', 3, 36, 'Dibuat', ''),
(62, 'PLETINA ARTICULACION', 3, 36, 'Dibuat', ''),
(63, 'EJE POSICIONADOR', 3, 36, 'Dibuat', ''),
(64, 'ANILLA', 3, 36, 'Dibuat', ''),
(65, 'CAM STOP POSITION', 3, 36, 'Dibuat', ''),
(66, 'DATUM PLATE', 2, 37, 'Dibuat', ''),
(67, 'JIG BASE', 2, 37, 'Dibuat', ''),
(68, 'LOC PIN DIAMOND', 2, 37, 'Dibuat', ''),
(69, 'LOCATION PIN', 2, 37, 'Dibuat', ''),
(70, 'M BOTTOM PLATE', 2, 37, 'Dibuat', ''),
(71, 'MOUNTING PLATE', 2, 37, 'Dibuat', ''),
(72, 'PLATE SPRING', 2, 37, 'Dibuat', ''),
(73, 'BOTTOM PLATE', 1, 38, 'Dibuat', ''),
(74, 'DIES', 1, 38, 'Dibuat', ''),
(75, 'PUNCHER', 1, 38, 'Dibuat', ''),
(76, 'TOP PLATE', 1, 38, 'Dibuat', ''),
(77, 'PUNCHER HOLE', 1, 38, 'Dibuat', ''),
(78, 'STRIPER', 1, 38, 'Dibuat', ''),
(79, 'BUSHING', 1, 38, 'Dibuat', ''),
(80, 'PIN 1', 1, 38, 'Dibuat', ''),
(81, 'PIN 2', 1, 38, 'Dibuat', ''),
(82, 'UPPER PLATE', 2, 39, 'Dibuat', ''),
(83, 'MAIN PLATE SMPV', 2, 39, 'Dibuat', ''),
(84, 'SIDE PLATE SMPV', 2, 39, 'Dibuat', ''),
(85, 'DATUM HOLDER SMPV', 2, 39, 'Dibuat', ''),
(86, 'GUIDE BORE SMPV', 2, 39, 'Dibuat', ''),
(87, 'GUIDE BOLT', 2, 39, 'Dibuat', ''),
(88, 'LEAF SPRING SMPV', 2, 39, 'Dibuat', ''),
(89, 'BLOCK PLATE SMPV', 2, 39, 'Dibuat', ''),
(90, 'BASE WELDING JIG', 1, 40, 'Dibuat', ''),
(91, 'BACK SHAFT', 1, 40, 'Dibuat', ''),
(92, 'CLAMP JIG', 3, 40, 'Dibuat', ''),
(93, 'STOPPER DC', 1, 40, 'Dibuat', ''),
(94, 'STOPPER D14 FOR JIG NEW', 1, 40, 'Dibuat', ''),
(95, 'ADJUSTER BLOCK D14N', 6, 40, 'Dibuat', ''),
(96, 'BUSH BAKELITE', 1, 40, 'Dibuat', ''),
(97, 'PLATE BAKELITE', 1, 40, 'Dibuat', ''),
(98, 'BAKE WASHER', 4, 40, 'Dibuat', ''),
(99, 'CENTER CORE', 10, 41, 'Dibuat', ''),
(100, 'CENTER CORE', 5, 42, 'Dibuat', ''),
(101, 'LOWER HOUSING', 3, 43, 'Dibuat', ''),
(102, 'UPPER HOUSING', 3, 43, 'Dibuat', ''),
(103, 'LOWER HOUSING', 3, 44, 'OK', ''),
(104, 'BS-045 INSERT JIG', 1, 45, 'Terkirim', ''),
(105, 'BT-062 PIN SQUEEZING', 1, 46, 'Terkirim', ''),
(106, 'BLOCK', 1, 47, 'Terkirim', ''),
(107, 'KOKEN', 20, 48, 'Dibuat', ''),
(108, 'CENTER CORE', 5, 49, 'Dibuat', ''),
(109, 'SIDE A', 1, 50, 'Dibuat', ''),
(110, 'GATE SIDE A', 1, 50, 'Dibuat', ''),
(111, 'SUPPORT SIDE A', 1, 50, 'Dibuat', ''),
(112, 'BOLT ADJUSTER', 6, 50, 'Dibuat', ''),
(113, 'STOPPER SLIDER 1', 1, 50, 'Dibuat', ''),
(114, 'STOPPER SLIDER 2', 1, 50, 'Dibuat', ''),
(115, 'STOPPER SLIDER 3', 1, 50, 'Dibuat', ''),
(116, 'SLIDER', 1, 50, 'Dibuat', ''),
(117, 'CENTER CORE', 1, 50, 'Dibuat', ''),
(118, 'EXTENDED PIN', 1, 50, 'Dibuat', ''),
(119, 'LOWER HOUSING MODIF', 1, 50, 'Dibuat', ''),
(120, 'UPPER HOUSING', 1, 50, 'Dibuat', ''),
(121, 'SIDE B', 1, 50, 'Dibuat', ''),
(122, 'GATE SIDE B', 1, 50, 'Dibuat', ''),
(123, 'SUPPORT SIDE B', 1, 50, 'Dibuat', ''),
(124, 'RING', 8, 50, 'Dibuat', ''),
(125, 'ANGULAR PIN', 1, 50, 'Dibuat', ''),
(126, 'INSERT HOLDER MC 01S', 1, 50, 'Dibuat', ''),
(127, 'EYE BOLT', 2, 50, 'Dibuat', ''),
(128, 'EYE BOLT', 1, 50, 'Dibuat', ''),
(129, 'GUIDE PIN 1', 4, 50, 'Dibuat', ''),
(130, 'HOUSING PIN', 1, 50, 'Dibuat', ''),
(131, 'SIDE A', 1, 51, 'Dibuat', ''),
(132, 'GATE SIDE A', 1, 51, 'Dibuat', ''),
(133, 'HOUSING PIN', 1, 51, 'Dibuat', ''),
(134, 'GUIDE PIN 1', 4, 51, 'Dibuat', ''),
(135, 'SUPPORT SIDE A', 1, 51, 'Dibuat', ''),
(136, 'BOLT ADJUSTER ', 6, 51, 'Dibuat', ''),
(137, 'STOPPER SLIDER 1', 1, 51, 'Dibuat', ''),
(138, 'STOPPER SLIDER 2', 1, 51, 'Dibuat', ''),
(139, 'STOPPER SLIDER 3', 1, 51, 'Dibuat', ''),
(140, 'SLIDER', 1, 51, 'Dibuat', ''),
(141, 'CENTER CORE', 1, 51, 'Dibuat', ''),
(142, 'EXTENDED PIN', 1, 51, 'Dibuat', ''),
(143, 'LOWER HOUSING MODIF', 1, 51, 'Dibuat', ''),
(144, 'UPPER HOUSING', 1, 51, 'Dibuat', ''),
(145, 'SIDE B', 1, 51, 'Dibuat', ''),
(146, 'GATE SIDE B', 1, 51, 'Dibuat', ''),
(147, 'SUPPORT SIDE B', 1, 51, 'Dibuat', ''),
(148, 'RING', 8, 51, 'Dibuat', ''),
(149, 'ANGULAR PIN', 1, 51, 'Dibuat', ''),
(150, 'INSERT HOLDER MC 01S', 1, 51, 'Dibuat', ''),
(151, 'EYE BOLT', 2, 51, 'Dibuat', ''),
(152, 'EYE BOLT', 1, 51, 'Dibuat', ''),
(153, 'PIN CORE', 20, 52, 'Dibuat', ''),
(154, 'EJECTOR PIN 7', 30, 52, 'Dibuat', ''),
(155, 'PUNCH CAULKING BA3', 1, 53, 'Dibuat', ''),
(156, 'HOUSE JIG', 1, 54, 'Dibuat', ''),
(157, 'AS JIG', 1, 54, 'Dibuat', ''),
(158, 'BEARING OD42', 3, 54, 'Dibuat', ''),
(159, 'TUTUP HOUSING', 1, 54, 'Dibuat', ''),
(160, 'MUR', 1, 54, 'Dibuat', ''),
(161, 'CLAMPING PART', 1, 54, 'Dibuat', ''),
(162, 'UPPER HOUSING', 1, 44, 'Dibuat', ''),
(166, 'LOWER HOUSING', 1, 44, 'OK', 'NG');

-- --------------------------------------------------------

--
-- Table structure for table `proses`
--

CREATE TABLE `proses` (
  `id` int(11) NOT NULL,
  `singkatan` varchar(5) DEFAULT NULL,
  `mesin` varchar(100) NOT NULL,
  `harga` bigint(20) NOT NULL,
  `subcon` int(11) NOT NULL,
  `status` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `proses`
--

INSERT INTO `proses` (`id`, `singkatan`, `mesin`, `harga`, `subcon`, `status`) VALUES
(2, 'H', 'Harden', 60000, 1, 'Aktif'),
(4, 'CB', 'CNC Bubut', 175000, 0, 'Aktif'),
(5, 'CM', 'CNC Milling', 175000, 0, 'Aktif'),
(6, 'MM', 'Miling Manual', 90000, 0, 'Aktif'),
(7, 'BM', 'Bubut Manual', 90000, 0, 'Aktif'),
(8, 'GS', 'Grinding Surface', 100000, 0, 'Aktif'),
(9, 'GC', 'Grinding Cylinder', 120000, 1, 'Aktif'),
(10, 'EDM', 'EDM', 100000, 0, 'Aktif'),
(11, 'W', 'Welding', 100000, 0, 'Aktif'),
(12, 'WC', 'Wire Cut', 90000, 1, 'Aktif'),
(13, 'LW', 'Laser Welding', 90000, 1, 'Aktif'),
(14, 'LC', 'Laser Cutting', 100000, 1, 'Aktif'),
(15, 'H', 'Harden', 100000, 1, 'Tidak Aktif'),
(16, 'P', 'Plating', 5000, 1, 'Aktif'),
(17, 'B', 'Blacken', 8000, 1, 'Aktif'),
(18, 'HR', 'Hardenrome', 100000, 1, 'Aktif'),
(19, 'C', 'Chrome', 100000, 1, 'Aktif'),
(20, 'M', 'Machining', 100000, 1, 'Aktif'),
(21, 'F', 'Fabrikasi', 100000, 1, 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `proyek`
--

CREATE TABLE `proyek` (
  `id` int(11) NOT NULL,
  `order_date` varchar(8) NOT NULL,
  `customer` int(11) NOT NULL,
  `nama_proyek` varchar(200) NOT NULL,
  `codewo` varchar(8) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `cs` varchar(8) NOT NULL,
  `status` varchar(30) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `targetdeliv` varchar(8) NOT NULL,
  `actualdeliv` varchar(8) DEFAULT NULL,
  `po` int(11) DEFAULT NULL,
  `nopo` varchar(15) DEFAULT NULL,
  `tipe_proyek` varchar(50) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `status_proyek` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `proyek`
--

INSERT INTO `proyek` (`id`, `order_date`, `customer`, `nama_proyek`, `codewo`, `tahun`, `cs`, `status`, `keterangan`, `targetdeliv`, `actualdeliv`, `po`, `nopo`, `tipe_proyek`, `jumlah`, `status_proyek`) VALUES
(36, '20200331', 10, 'MANDRIL STOP POSITION', 'P01-169', '2020', '', 'Aktif', '3 SET', '20200422', NULL, NULL, NULL, NULL, NULL, NULL),
(37, '20200401', 7, 'ASSY JIG CASETTE 0P-20', 'A01-171', '2020', 'CS-0381', 'Aktif', '2 LOT', '20200410', NULL, NULL, NULL, NULL, NULL, NULL),
(38, '20200401', 15, 'PUNCHING JIG', 'P04-176', '1970', '', 'Aktif', '1 UNIT', '20200408', NULL, 14000000, NULL, 'machining', NULL, NULL),
(39, '20200401', 7, 'ASSY JIG CASETTE OP-30', 'A01-172', '2020', 'CS-0384', 'Aktif', '2 LOT', '20200410', NULL, NULL, NULL, NULL, NULL, NULL),
(40, '20200401', 7, 'ASSY JIG WELD PA-D14N', 'A01-170', '2020', 'CS-0375', 'Aktif', '1 UNIT', '20200410', NULL, NULL, NULL, NULL, NULL, NULL),
(41, '20200402', 7, 'CENTER CORE (MC08A)', 'A01-180', '2020', 'CS-0401', 'Aktif', '10 PCS', '20200410', NULL, NULL, NULL, NULL, NULL, NULL),
(42, '20200402', 7, 'CENTER CORE (MC01A RR)', 'A01-181', '2020', 'CS-0402', 'Aktif', '5 PCS', '20200410', NULL, NULL, NULL, NULL, NULL, NULL),
(43, '20200402', 7, 'ASSY LOWER UPPER HOUSING (MC08A)', 'A01-182', '2020', 'CS-0406', 'Aktif', '3 PCS', '20200412', NULL, NULL, NULL, NULL, NULL, NULL),
(44, '20200402', 7, 'LOWER HOUSING MODIF (MC01A LH)', 'A01-183', '2020', 'CS-0407', 'Aktif', '3 PCS', '20200412', NULL, NULL, NULL, 'machining', NULL, NULL),
(45, '20200401', 8, 'BS-045 INSERT JIG', 'T01-173', '1970', '', 'Aktif', '1 PCS', '20200403', '20200403', 1435000, NULL, NULL, NULL, NULL),
(46, '20200401', 8, 'BT-062 PIN SQUEEZING', 'T01-174', '1970', '', 'Aktif', '1 PCS', '20200403', '20200403', 250000, NULL, NULL, NULL, NULL),
(47, '20200401', 16, 'BLOCK (JASA EDM)', 'H01-175', '1970', '', 'Aktif', '1 PCS', '20200402', '20200403', 80000, NULL, NULL, NULL, NULL),
(48, '20200402', 7, 'KOKEN', 'A01-178', '2020', 'CS-0388', 'Aktif', '20 PCS', '20200415', NULL, NULL, NULL, NULL, NULL, NULL),
(49, '20200407', 7, 'CENTER CORE (MC04H)', 'A01-186', '2020', 'CS-0413', 'Aktif', '5 PCS', '20200515', NULL, NULL, NULL, NULL, NULL, NULL),
(50, '20200409', 7, 'DIES GRAVITY MC 01S', 'A01-188', '2020', 'CS-0194', 'Aktif', '1 UNIT', '20200515', NULL, NULL, NULL, NULL, NULL, NULL),
(51, '20200409', 7, 'DIES GRAVITY MC 01S', 'A01-189', '2020', 'CS-0195', 'Aktif', '1 UNIT', '20200515', NULL, NULL, NULL, NULL, NULL, NULL),
(52, '20200409', 7, 'DIES GRAVITY BC 08G RR', 'A01-190', '2020', 'CS-0418', 'Aktif', '1 LOT', '20200515', NULL, NULL, NULL, NULL, NULL, NULL),
(53, '20200409', 7, 'PUNCH CAULKING BA3', 'A01-191', '2020', 'CS-0419', 'Aktif', '1 PCS', '20200515', NULL, NULL, NULL, NULL, NULL, NULL),
(54, '20200414', 8, 'JIG BG-001A', 'T01-193', '2020', '', 'Aktif', '1 UNIT', '20200430', NULL, NULL, NULL, NULL, NULL, NULL),
(55, '20200421', 9, 'rak dies', 'A03-123', '2020', '', 'Aktif', '1 UNIT', '20200509', NULL, NULL, NULL, 'fabrikasi', NULL, NULL),
(62, '20200710', 14, 'RAK DIES', 'T01007', '2020', 'FS', 'Aktif', 'Unit', '20200730', NULL, NULL, NULL, 'fabrikasi', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subcon`
--

CREATE TABLE `subcon` (
  `id` int(11) NOT NULL,
  `part` int(11) NOT NULL,
  `tgl_pengiriman` varchar(8) NOT NULL,
  `status` varchar(12) NOT NULL,
  `nosurat` int(11) NOT NULL,
  `vendor` int(11) NOT NULL,
  `proses` int(11) NOT NULL,
  `harga` varchar(50) NOT NULL,
  `keterangan` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `supplier` varchar(100) NOT NULL,
  `alamat` varchar(2000) NOT NULL,
  `status` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `supplier`, `alamat`, `status`) VALUES
(9, 'PT. COY STEEL', 'Pergudangan Prima Centre Extension Blok D No.42., RT.9/RW.2, Kedaung Kali Angke, Kecamatan Cengkareng, Kota Jakarta Barat', 'Aktif'),
(10, 'PT. TECHNO METAL INDUSTRY', 'JL. Jababeka VI B, Blok J Kavling 7 D, Kawasan Industri Jababeka Cikarang - Bekasi, 17530, Harja Mekar, Kec. Cikarang Utara, Bekasi, Jawa Barat 17530', 'Aktif'),
(11, 'CV. JAYA ABADI', 'Jl. Tegal Danas Jl. Binong No.Desa, Jayamukti, Kec. Cikarang Pusat, Bekasi, Jawa Barat 17531', 'Aktif'),
(12, 'PT. BINANGUN', 'Jl. Swadaya Jl. Buaran Utara No.Kp, RT.02/RW.01, Lambangsari, Kec. Tambun Sel., Bekasi, Jawa Barat 17510', 'Aktif'),
(13, 'PT. CHIYODA', 'Kawasan Industri Jababeka II, SFB Blok JJ No. 18, Cikarang, Pasirsari, Cikarang Sel., Bekasi, Jawa Barat 17530', 'Aktif'),
(14, 'PT. PUTRA TOOLSINDO', 'Kawasan Industri Jababeka 2 Jl. Industri Selatan 11 Blok EE No. 12A, Pasirsari, Cikarang Sel., Bekasi, Jawa Barat 17530', 'Aktif'),
(15, 'PT. MIJO INDAH', 'Jl Industri Slt 11 Kawasan Industri Jababeka Tahap II Bl EE/11-F', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `surat_jalan`
--

CREATE TABLE `surat_jalan` (
  `id` int(11) NOT NULL,
  `part` int(11) NOT NULL,
  `tgl_pengiriman` varchar(8) NOT NULL,
  `no_surat_jalan` varchar(5) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `surat_jalan`
--

INSERT INTO `surat_jalan` (`id`, `part`, `tgl_pengiriman`, `no_surat_jalan`, `keterangan`, `status`) VALUES
(44, 106, '20200403', '1', '', 'Terkirim'),
(45, 104, '20200403', '2', '', 'Terkirim'),
(46, 105, '20200403', '3', '', 'Terkirim');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `NPK` int(11) NOT NULL,
  `Nama` varchar(50) NOT NULL,
  `Password` varchar(12) NOT NULL,
  `Role` varchar(15) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Status` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`NPK`, `Nama`, `Password`, `Role`, `Email`, `Status`) VALUES
(51701, 'Wella', '1234', 'Admin Marketing', 'wella@gmail.com', 'Aktif'),
(51702, 'Tessa Wahani', '1234', 'Purchasing', 'Tessa@gmail.com', 'Aktif'),
(51703, 'Surya santoso', '1234', 'Engineering', 'Surya@gmail.com', 'Aktif'),
(51704, 'QC', '1234', 'Quality Control', 'qc@gmail.com', 'Aktif'),
(51705, 'Niki', '1234', 'Operator', 'operator@gmail.com', 'Aktif'),
(51780, 'Siti Nuraeni', '1234', 'Finance', 'Siti@gmail.com', 'Aktif'),
(51781, 'Admin', '1234', 'Admin', 'Admin@gmail.com', 'Aktif'),
(51795, 'Eko jarwad', '12345', 'Marketing', 'Indri1@gmail.com', 'Aktif'),
(51796, 'Yudi tri', '1234', 'Marketing', 'Agustinus@gmail.com', 'Aktif'),
(51797, 'Dhona anggoro', '1234', 'Marketing', 'Junia@gmail.com', 'Aktif'),
(51799, 'Andino', '1234', 'Foreman', 'a@gmail.com', 'Aktif');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimasi`
--
ALTER TABLE `estimasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_proses` (`proses`),
  ADD KEY `fk_part_estimasi` (`part`),
  ADD KEY `fk_fabrikasi_estimasi` (`material_fabrikasi`);

--
-- Indexes for table `flow`
--
ALTER TABLE `flow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `list_material`
--
ALTER TABLE `list_material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_material` (`material`),
  ADD KEY `fk_part_list` (`part`);

--
-- Indexes for table `list_material_fabrikasi`
--
ALTER TABLE `list_material_fabrikasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_proyek_fabrikasi` (`proyek`);

--
-- Indexes for table `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `part`
--
ALTER TABLE `part`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pk_proyek` (`proyek`);

--
-- Indexes for table `proses`
--
ALTER TABLE `proses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proyek`
--
ALTER TABLE `proyek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcon`
--
ALTER TABLE `subcon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_part_surat` (`part`),
  ADD KEY `fk_proses` (`proses`),
  ADD KEY `fk_vendor` (`vendor`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surat_jalan`
--
ALTER TABLE `surat_jalan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_surat` (`part`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`NPK`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `estimasi`
--
ALTER TABLE `estimasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1856;

--
-- AUTO_INCREMENT for table `flow`
--
ALTER TABLE `flow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `list_material`
--
ALTER TABLE `list_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `list_material_fabrikasi`
--
ALTER TABLE `list_material_fabrikasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `material`
--
ALTER TABLE `material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `part`
--
ALTER TABLE `part`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT for table `proses`
--
ALTER TABLE `proses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `proyek`
--
ALTER TABLE `proyek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `subcon`
--
ALTER TABLE `subcon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `surat_jalan`
--
ALTER TABLE `surat_jalan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `estimasi`
--
ALTER TABLE `estimasi`
  ADD CONSTRAINT `fk_fabrikasi_estimasi` FOREIGN KEY (`material_fabrikasi`) REFERENCES `list_material_fabrikasi` (`id`),
  ADD CONSTRAINT `fk_part_estimasi` FOREIGN KEY (`part`) REFERENCES `part` (`id`);

--
-- Constraints for table `list_material`
--
ALTER TABLE `list_material`
  ADD CONSTRAINT `fk_material` FOREIGN KEY (`material`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `fk_part_list` FOREIGN KEY (`part`) REFERENCES `part` (`id`);

--
-- Constraints for table `part`
--
ALTER TABLE `part`
  ADD CONSTRAINT `pk_proyek` FOREIGN KEY (`proyek`) REFERENCES `proyek` (`id`);

--
-- Constraints for table `subcon`
--
ALTER TABLE `subcon`
  ADD CONSTRAINT `fk_proses` FOREIGN KEY (`proses`) REFERENCES `proses` (`id`),
  ADD CONSTRAINT `fk_vendor` FOREIGN KEY (`vendor`) REFERENCES `supplier` (`id`);

--
-- Constraints for table `surat_jalan`
--
ALTER TABLE `surat_jalan`
  ADD CONSTRAINT `fk_surat` FOREIGN KEY (`part`) REFERENCES `part` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
