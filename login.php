<?php
include'koneksi.php';

if (isset($_POST['login'])) {
    session_start();
    if (empty($_POST['noid']) || empty($_POST['password'])  ) {
        echo "<script>alert('Gagal Masuk, periksa kembali NPK dan kata sandi !');
			window.history.go(-1);</script>";
    } else {
        // Variabel username dan password
        $noid = $_POST['noid'];
        $password = $_POST['password'];
        // Mencegah MySQL injection 
        $noid = stripslashes($noid);
        $password = stripslashes($password);
        // SQL query untuk memeriksa apakah karyawan terdapat di database?
        $query = mysqli_query($conn,"select a.NPK,a.Password, a.Role, a.Nama from user as a  where a.Password= '".$password."' AND a.NPK=".$noid." and a.Status = 'Aktif'");
        $rows = mysqli_fetch_array($query);
        if(isset($rows['Role']))
        {
            $_SESSION['role'] = $rows['Role'];
            $_SESSION['nama'] = $rows['Nama'];
            $_SESSION['npk'] = $rows['NPK'];
            if($_SESSION['role']=='Admin')
            {
                header("location: aplikasi/indexadmin.php");
            }else if($_SESSION['role']=='Foreman')
            {   
                header("location: aplikasi/berandaforeman.php");
            }
            else if($_SESSION['role']=='Purchasing')
            {
                header("location: aplikasi/berandapurchasing.php");
            }
            else if($_SESSION['role']=='Admin Marketing')
            {
                header("location: aplikasi/berandamarketing.php");
            }
            else if($_SESSION['role']=='Engineering')
            {
                header("location: aplikasi/berandaengineering.php");
            }
            else if($_SESSION['role']=='Quality Control')
            {
                header("location: aplikasi/berandaqc.php");
            }
            else if($_SESSION['role']=='Operator')
            {
                header("location: aplikasi/indexoperator.php");
            }
            else
            {
                header("location: aplikasi/beranda.php");
            }
            
        }
        else
        {

            echo "<script>alert('username atau password salah');</script>";
            echo "<script>
             window.history.go(-1);</script>";
        }
    }
}
