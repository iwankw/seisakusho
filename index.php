
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

<style type="text/css">
  body {
    padding-top: 90px;
    padding-bottom: 20px;
    padding-left: 20px;
    padding-right: 20px;
  }

  .c-nav-static-top {
      top: 0;
      left: 0;
      right: 0;
      position: fixed;
      height: 70px;
      width: 100% !important;
      padding: 10px 20px 10px 20px;
      box-shadow: 0px 2px 0px 0px #EEE;
      background-color: white;
      z-index: 4;
  }
  .c-nav-static-bottom {
      bottom: 0;
      left: 0;
      right: 0;
      position: fixed;
      height: 50px;
      opacity: 0.9 !important;
      width: 100% !important;
      padding: 12px 5px 5px 5px;
      font-size: 12px;
      box-shadow: 0px 2px 0px 0px #EEE;
      background-color: white;
      z-index: 4;
      text-align: center;
  }
  
  .c-form-login {
      width: 100%;
      background-color: white;
      padding: 30px !important;
      border: 3px #EEE solid;
  }
  .c-form-login {
        position: fixed;
        width: 400px;
        right: 75px;
        margin: 30px;
        background-color: white;
  }
  .form-group{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-flex:0;-webkit-flex:0 0 auto;-ms-flex:0 0 auto;flex:0 0 auto;-webkit-flex-flow:row wrap;-ms-flex-flow:row wrap;flex-flow:row wrap;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;margin-bottom:0}
  .form-control{
    border-color:#5cb85c;
    display:inline-block;
    width:auto;
    vertical-align:middle;
    display:block;width:100%;padding:.5rem .75rem;font-size:1rem;line-height:1.25;color:#464a4c;background-color:#fff;background-image:none;-webkit-background-clip:padding-box;background-clip:padding-box;border:1px solid rgba(0,0,0,.15);
  }
  label {
    color: black; 
    display: inline-block;
    margin-bottom: .5rem;

  }
  .float-left{float:left!important}
  .btn-primary{color:#fff;background-color:#0275d8;border-color:#0275d8}
  .btn {

      display: inline-block;
      font-weight: 400;
      line-height: 1.25;
      text-align: center;
      vertical-align: middle;
      border: 1px solid transparent;
          border-top-color: transparent;
          border-right-color: transparent;
          border-bottom-color: transparent;
          border-left-color: transparent;
      padding: .5rem 1rem;
      font-size: 1rem;

  }
</style>
</head>
<body style="background-image: url('assets/img/latar1.jpg'); background-repeat: no-repeat; background-size: cover;">
    <form method="post" action="login.php" id="form1" enctype="multipart/form-data">
  
      <div>
        
        <div class="c-nav-static-top" style="opacity: 0.9;">
          
          <div class="float-left">
              <img src="assets/img/logo.jpg" height="50px" width="200px">
              <!-- <span>Job Application Data Management Information System ( JASMINS )</span></a> -->
          </div>
          
        </div> 
        <div class="c-form-login">
          <span style="font-size: 24px;color: black;border: none;display: inline-block;">Login</span>
          <hr>
          <div class="form-group">
            <label for="txtUsername">NPK <span style="color: red;">*</span></label>
            <span id="" style="color:Red;display:none;"> harus diisi</span>
            <input required="true" name="noid" type="text" id="txtUsername" class="form-control">
          </div>
          <div class="form-group">
            <label for="txtPassword">Kata Sandi <span style="color: red;">*</span></label>
            <span id="" style="color:Red;display:none;"> harus diisi</span>
            <input required="true" name="password" type="password" id="txtPassword" class="form-control">
          </div>
          <input type="submit" name="login" value="Login" id="login" class="btn btn-primary" style="width: 100%; margin-top: 10px; margin-bottom: 10px;">
          <!-- <span style="margin-top: 10px;">Lupa Password? <a href="LupaPassword.php">Klik disini</a>.</span> -->
        </div>
            
        <div class="c-nav-static-bottom">
        <?php $tgl=date('Y');?>
          Copyright © -CAN- <?php echo $tgl;?> - 
        </div>
            
      </div>
    </form>
</body>
</html> 