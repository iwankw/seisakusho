<?php
	include "../koneksi.php";
	session_start();
	if($_SESSION['role']!= "Quality Control")
	{
		echo "<script>  window.location.href= '../index.php'; </script>";
	}
	// Delete
	
?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Dec 2019 18:38:13 GMT -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Project</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="../assets/img/icon.ico" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="../assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['../assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/atlantis.css">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="../assets/css/demo.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>
<body>
	<div class="wrapper sidebar_minimize">
		<div class="main-header">
			<!-- Logo Header -->
			<div class="logo-header" data-background-color="blue">
				
				<a href="indexadmin.php" class="logo">
					<img src="../assets/img/logo.jpg" width="160px" alt="navbar brand" class="navbar-brand">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
				<div class="nav-toggle">
					<button class="btn btn-toggle toggle-sidebar">
						<i class="icon-menu"></i>
					</button>
				</div>
			</div>
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
				
				<div class="container-fluid">
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item toggle-nav-search hidden-caret">
							<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
								<i class="fa fa-search"></i>
							</a>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link dropdown-toggle" href="#" id="notifDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-bell"></i>
								<span class="notification">4</span>
							</a>
							<ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
								<li>
									<div class="dropdown-title">You have 4 new notification</div>
								</li>
								<li>
									<div class="notif-scroll scrollbar-outer">
										<div class="notif-center">
											<a href="#">
												<div class="notif-icon notif-primary"> <i class="fa fa-user-plus"></i> </div>
												<div class="notif-content">
													<span class="block">
														New user registered
													</span>
													<span class="time">5 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-icon notif-success"> <i class="fa fa-comment"></i> </div>
												<div class="notif-content">
													<span class="block">
														Rahmad commented on Admin
													</span>
													<span class="time">12 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-img"> 
													<img src="../assets/img/profile2.jpg" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="block">
														Reza send messages to you
													</span>
													<span class="time">12 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-icon notif-danger"> <i class="fa fa-heart"></i> </div>
												<div class="notif-content">
													<span class="block">
														Farrah liked Admin
													</span>
													<span class="time">17 minutes ago</span> 
												</div>
											</a>
										</div>
									</div>
								</li>
								<li>
									<a class="see-all" href="javascript:void(0);">See all notifications<i class="fa fa-angle-right"></i> </a>
								</li>
							</ul>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<img src="../assets/img/profile.jpg" alt="..." class="avatar-img rounded-circle">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<div class="user-box">
											<div class="avatar-lg"><img src="../assets/img/profile.jpg" alt="image profile" class="avatar-img rounded"></div>
											<div class="u-text">
												<h4><?php echo $_SESSION["nama"]; ?></h4>
												<a href="profile.php" class="btn btn-xs btn-secondary btn-sm">Ubah Profil</a>
											</div>
										</div>
									</li>
									<li>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="../index.php">Logout</a>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>

		<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="../assets/img/profile.jpg" alt="..." class="avatar-img rounded-circle">
						</div>
						<div class="info">
							<a data-toggle="collapse" href="#" aria-expanded="true">
								<span>
									<?php echo $_SESSION["nama"]; ?>
									<span class="user-level"><?php echo $_SESSION["role"]; ?></span>
									<span class="caret"></span>
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">
									<li>
										<a href="ubahprofil.php">
											<span class="link-collapse">Ubah Profil</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav nav-primary">
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-menu"></i>
							</span>
							<h4 class="text-section">Menu</h4>
						</li>
						<li class="nav-item">
							<a href="berandaqc.php">
								<i class="fas fa-window-maximize"></i>
								<p>Beranda</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="indexprocessengineering.php">
								<i class="fas fa-window-maximize"></i>
								<p>Proses</p>
							</a>
						</li>
						<li class="nav-item active">
							<a href="indexreport.php">
								<i class="fas fa-window-maximize"></i>
								<p>Report</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="qualitycontrol.php">
								<i class="fas fa-window-maximize"></i>
								<p>Quality Control</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->

		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">Menu Project</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="indexprocessengineering.php">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="indexprocessengineering.php">Forms</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="indexprocessengineering.php">Project</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title pull-left">Tabel Project</div>
									
								</div>
								<div class="card-body">
									<div class="row">
										
											<div class="col-md-12 col-lg-12 col-s-4">
												<div class="table-responsive">
													<table id="basic-datatables" class="display table table-striped table-hover" style="font-size: 12px;">
														<thead>
															<tr>
																<th style="text-align: center;">NO</th>
																<th style="text-align: center;">NAMA CUSTOMER</th>
																<th style="text-align: center;">NAMA PROJECT</th>
																<th style="text-align: center;">CODE WO</th>
																<th style="text-align: center;width: 70px;">CS</th>
																<th style="text-align: center;">PRODUK NAME</th>
																<th style="text-align: center;">STATUS</th>
																<th style="text-align: center;">TANGGAL DELIVERY</th>
																<th style="text-align: center;width: 100px;">AKSI</th>
															</tr>
														</thead>
														<tbody>
															<?php
							                                    $view = mysqli_query($conn, "select a.*, b.customer as cust from proyek as a join customer as b on a.customer = b.id where a.status = 'Aktif'");
							                                    $a =0;
							                                    $row = mysqli_fetch_array($view);
							                                    while ($row = mysqli_fetch_array($view)) {
							                                    	$a++;
							                                ?>
							                                    	<tr>
							                                    		<td><?php echo $a;?></td>
																		<td ><?php echo $row['cust'];?></td>
																		<td ><?php echo $row['nama_proyek'];?></td>
																		<td><?php echo $row['codewo'];?></td>
																		<td><?php echo $row['cs'];?> </td>
																		<td><?php echo $row['nama_proyek'];?> </td>
																		</td>
																		<td><?php echo $row['status'];?></td>
																		<td><?php echo date("d F Y", strtotime($row['targetdeliv']));
																		?></td>	
																		
																		<td>
																			<?php 
																				$button_query = mysqli_query($conn, "select a.id from list_material as a join part as b on a.part = b.id join proyek as c on c.id = b.proyek where c.id = ".$row['id']."");
																				$cek = 0;
											                                    while ($row_part = mysqli_fetch_array($button_query)) {
							                                    					$cek = 1;
							                                    				}
							                                    				$button_query = mysqli_query($conn, "select a.id from list_material_fabrikasi as a join proyek as c on c.id = a.proyek where c.id = ".$row['id']." and a.status != 'Draft'");
																				$cek2 = 0;
											                                    while ($row_part = mysqli_fetch_array($button_query)) {
							                                    					$cek2 = 1;
							                                    				}
							                                    			?> 
							                                    			<?php 
								                                    			if($cek == 1 )
								                                    			{
								                                    		?>
								                                    			<button class="btn btn-icon btn-round btn-info" onclick="ubah(<?php echo  $row["id"]; ?>)" >
																					<i class="fa fa-list" title="Estimation"></i>
																				</button>
																				<button class="btn btn-icon btn-round btn-info" onclick="export_excel(<?php echo  $row["id"]; ?>)" >
																					<i class="fa fa-download" title="Estimation"></i>
																				</button>
								                                    		<?php
								                                    			}	
							                                    			?>
																			<?php 
								                                    			 if($cek2 == 1 )
								                                    			{
								                                    		?>
								                                    			<button class="btn btn-icon btn-round btn-info" onclick="ubah2(<?php echo  $row["id"]; ?>)" >
																					<i class="fa fa-list" title="Estimation"></i>
																				</button>
								                                    		<?php
								                                    			}	
							                                    			?>	
																				
																		</td>
																	</tr>
															<?php
							                                   	}
							                                ?>
																	
														</tbody>
													</table>
												</div>
											</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">
					</nav>
					<div class="copyright ml-auto">
						2020, by <a href="#">CAN</a>
					</div>				
				</div>
			</footer>
		</div>
		
		
		<!-- Custom template | don't include it in your project! -->
		
		<!-- End Custom template -->
	</div>
	<!--   Core JS Files   -->
	<script src="../assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="../assets/js/core/popper.min.js"></script>
	<script src="../assets/js/core/bootstrap.min.js"></script>

	<!-- jQuery UI -->
	<script src="../assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="../assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="../assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>

	<!-- Moment JS -->
	<script src="../assets/js/plugin/moment/moment.min.js"></script>

	<!-- Chart JS -->
	<script src="../assets/js/plugin/chart.js/chart.min.js"></script>

	<!-- jQuery Sparkline -->
	<script src="../assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

	<!-- Chart Circle -->
	<script src="../assets/js/plugin/chart-circle/circles.min.js"></script>

	<!-- Datatables -->
	<script src="../assets/js/plugin/datatables/datatables.min.js"></script>

	<!-- Bootstrap Notify -->
	<!-- <script src="../assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script> -->

	<!-- Bootstrap Toggle -->
	<script src="../assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js"></script>

	<!-- jQuery Vector Maps -->
	<script src="../assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
	<script src="../assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script>

	<!-- Google Maps Plugin -->
	<script src="../assets/js/plugin/gmaps/gmaps.js"></script>

	<!-- Dropzone -->
	<script src="../assets/js/plugin/dropzone/dropzone.min.js"></script>

	<!-- Fullcalendar -->
	<script src="../assets/js/plugin/fullcalendar/fullcalendar.min.js"></script>

	<!-- DateTimePicker -->
	<script src="../assets/js/plugin/datepicker/bootstrap-datetimepicker.min.js"></script>

	<!-- Bootstrap Tagsinput -->
	<script src="../assets/js/plugin/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>

	<!-- Bootstrap Wizard -->
	<script src="../assets/js/plugin/bootstrap-wizard/bootstrapwizard.js"></script>

	<!-- jQuery Validation -->
	<script src="../assets/js/plugin/jquery.validate/jquery.validate.min.js"></script>

	<!-- Summernote -->
	<script src="../assets/js/plugin/summernote/summernote-bs4.min.js"></script>

	<!-- Select2 -->
	<script src="../assets/js/plugin/select2/select2.full.min.js"></script>

	<!-- Sweet Alert -->
	<script src="../assets/js/plugin/sweetalert/sweetalert.min.js"></script>

	<!-- Owl Carousel -->
	<script src="../assets/js/plugin/owl-carousel/owl.carousel.min.js"></script>

	<!-- Magnific Popup -->
	<script src="../assets/js/plugin/jquery.magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- Atlantis JS -->
	<script src="../assets/js/atlantis.min.js"></script>

	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script >
		function ubah(npk)
		{
			window.location.href = "detailreport.php?proyek="+npk;
		}
		function export_excel(npk)
		{
			window.location.href = "export_excel.php?proyek="+npk;
		}
		function ubah2(npk)
		{
			window.location.href = "tambahestimasifabrikasi.php?proyek="+npk;
		}
		function material(npk)
		{
			window.location.href = "indexlistmaterial.php?idubah="+npk;
		}
		function konfirmasi(npk) {
			event.preventDefault(); // prevent form submit
			var form = event.target.form; // storing the form
			swal({
				title: "Apakah Anda yakin ingin menghapus data?",
				text : "",
				type: "warning",
				buttons:{
						confirm: {
							text : 'Hapus',
							className : 'btn btn-success'
						},
						cancel: {
							visible: true,
							text : 'Batal',
							className: 'btn btn-danger'
						}      			
						
					}
				}).then((willDelete) => {
						if (willDelete) {
							swal({
								title: "Data Berhasil Dihapus !",
								icon: "success",
								buttons: {
									confirm: {
										text: "OK",
										value: true,
										visible: true,
										className: "btn btn-success",
										closeModal: true
									}
								}
							}).then(
								function() {
									window.location.href = "indexproyek.php?iddelete="+npk;
								}
							);
							
						} else {
							swal("Data tidak dihapus", {
								buttons : {
									confirm : {
										className: 'btn btn-info'
									}
								}
							});
						}
					});
		}
		$(document).ready(function() {
			$('#basic-datatables').DataTable({
			});

			$('#multi-filter-select').DataTable( {
				"pageLength": 5,
				initComplete: function () {
					this.api().columns().every( function () {
						var column = this;
						var select = $('<select class="form-control"><option value=""></option></select>')
						.appendTo( $(column.footer()).empty() )
						.on( 'change', function () {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
								);

							column
							.search( val ? '^'+val+'$' : '', true, false )
							.draw();
						} );

						column.data().unique().sort().each( function ( d, j ) {
							select.append( '<option value="'+d+'">'+d+'</option>' )
						} );
					} );
				}
			});

			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});

			var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

			$('#addRowButton').click(function() {
				$('#add-row').dataTable().fnAddData([
					$("#addName").val(),
					$("#addPosition").val(),
					$("#addOffice").val(),
					action
					]);
				$('#addRowModal').modal('hide');

			});
		});
		
	</script>	
</body>

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Dec 2019 18:39:00 GMT -->
</html>