<?php
	include "../koneksi.php";
	session_start();
	// if($_SESSION['role']!= "Operator")
	// {
	// 	echo "<script>  window.location.href= '../index.php'; </script>";
	// }
	
	if(isset($_GET['berhasil'])){
		echo '<script> swal({
        		title : "Data Berhasil Disimpan !",
				icon : "success",
				buttons: {        			
					confirm: {
						className : "btn btn-primary"
					}
				},
			}); </script>';
	}
	if(isset($_GET['id'])){
		$id = $_GET['id'];
		$view = mysqli_query($conn,"SELECT a.id, a.partname, a.qty, b.codewo FROM part as a join proyek as b on a.proyek = b.id WHERE a.id = ".$id."");
		$sel = mysqli_fetch_array($view);
	}
	
	
    if (isset($_POST['idpart'])) {
    	date_default_timezone_set("Asia/Bangkok");
    	$jumlahflowpart =0; //untuk menampung jumlah proses dari suatu part
    	$part = $_POST['idpart'];
    	$proses = $_POST['proses']; 
    	$tipe = $_POST['tipe'];
    	$tipeproyek = $_POST['tipeproyek'];
    	$jumlahpart =0;
    	$jumlahflowpart = 0;
    	$idestimasi=0;
    	$jumlahflow =0;
    	$jumlahflowscan =0; //menampung jumlah flow proses yang sudah dijalani
    	if($tipeproyek=='machining'){
    		// get jumlah part
	    	$view = mysqli_query($conn,"SELECT qty from part as a where a.id = $part " );
			$sel = mysqli_fetch_array($view);
			$jumlahpart = $sel['qty'];
			// get jumlah proses dari part
	    	$view = mysqli_query($conn,"SELECT COUNT(id) as jumlah from estimasi as a where a.part = $part " );
			$sel = mysqli_fetch_array($view);
			$jumlahflowpart = $sel['jumlah'];

			// get estimasi
			$view = mysqli_query($conn,"SELECT a.id FROM estimasi as a WHERE a.part = $part and a.proses = $proses " );
			$sel = mysqli_fetch_array($view);
			$idestimasi = $sel['id'];
			// get jumlah data di flow
			
			$view = mysqli_query($conn,"SELECT a.estimasi from flow as a left join estimasi as b on a.estimasi = b.id where b.part = $part GROUP by a.estimasi " );
			$jumlahflowscan = mysqli_num_rows($view);
    	}
    	
		

		if($_POST['tipe']=='1'){
			$view = mysqli_query($conn,"SELECT COUNT(id) as jumlah from flow as a where a.estimasi = $idestimasi and a.scan_in != ''" );
			$sel = mysqli_fetch_array($view);
			$jumlahflow = $sel['jumlah'];
		}else{
			$view = mysqli_query($conn,"SELECT COUNT(id) as jumlah from flow as a where a.estimasi = $idestimasi and a.scan_out != ''" );
			$sel = mysqli_fetch_array($view);
			$jumlahflow = $sel['jumlah'];
		}

		// Jumlah scan in scan out
		$view = mysqli_query($conn,"SELECT COUNT(id) as jumlah from flow as a where a.estimasi = $idestimasi and a.scan_in != ''" );
		$sel = mysqli_fetch_array($view);
		$scan_in = $sel['jumlah'];
		$view = mysqli_query($conn,"SELECT COUNT(id) as jumlah from flow as a where a.estimasi = $idestimasi and a.scan_out != ''" );
		$sel = mysqli_fetch_array($view);
		$scan_out = $sel['jumlah'];
		// simpan
		if($jumlahpart>$jumlahflow){
			$waktu = date("Y-m-d H:i:s");
	    	if($_POST['tipe']=='1'){
	    		$view = mysqli_query($conn,"insert into flow(estimasi,scan_in) values(".$idestimasi.",'$waktu') ") or mysqli_error($conn);
	    	}else{
	    		if($scan_out<$scan_in){
	    			if($jumlahflowscan == $jumlahflowpart)
		    		{
		    			$view = mysqli_query($conn,"insert into flow(estimasi,scan_out) values(".$idestimasi.",'$waktu') ") or mysqli_error($conn);
		    			$view = mysqli_query($conn,"update part set status = 'Selesai' where id = ".$part." ") or mysqli_error($conn);
		    			// echo "<script> alert('Got!'); window.location.href= 'scanoperator.php?id=".$part."'; </script>";
		    		}else{
		    			$view = mysqli_query($conn,"insert into flow(estimasi,scan_out) values(".$idestimasi.",'$waktu') ") or mysqli_error($conn);	
		    		}
	    		}
		    	else{
		    		echo "<script> alert('Lakukan scan in terlebih dahulu sebelum scan out!'); window.location.href= 'scanoperator.php?id=".$part."'; </script>";
		    	}
	    		
	    	}
	    	echo "<script> alert('Data berhasil di simpan!'); window.location.href= 'scanoperator.php?id=".$part."'; </script>";
		}else{
			echo "<script> alert('Jumlah Scan Tidak Bisa Lebih Dari Jumlah Part!'); window.location.href= 'scanoperator.php?id=".$part."'; </script>";
	    	
    	}
	}
?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Dec 2019 18:38:13 GMT -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Project</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="../assets/img/icon.ico" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="../assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['../assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/atlantis.css">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="../assets/css/demo.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	
</head>
<body>
	
	<div class="row">
		<div class="col-md-3 col-lg-3"></div>
		<div class="col-md-6 col-lg-6 col-sm-12">
			<div class="card">
				<div class="card-header">
					<!-- <div class="card-title pull-left">Tabel Project</div> -->
				</div>
				<div class="card-body">
					<form action="scanoperator.php" method="POST">
						<center><div class="col-sm-6">
							<video id="preview" class="p-1 border" style="width:100%;"></video>
						</div></center>
						<div class="form-group">
							<label>Part <span class="required-label">*</span></label>
							<div class="input-group">
								<input name="part" id="part" type="text" required readonly class="form-control" value=""> &nbsp;
								<button name="scan" id="scan" type="button" class="btn btn-primary" onclick="scanqr(0)">Front Camera</button>&nbsp;
								<button name="scan" id="scan" type="button" class="btn btn-primary" onclick="scanqr(1)">Back Camera</button>	
								<!-- <button onclick="scanqr()" type="button">Click me</button> -->
								<!-- <input name="part" id="part" type="text" required readonly class="form-control" value="<?php echo $sel['partname'].' - '.$sel['codewo']; ?>"> -->
								<input name="idpart" id="idpart" type="hidden" required readonly class="form-control" value="<?php echo $sel['id']; ?>">
								<input name="tipeproyek" id="tipeproyek" type="hidden" >
							</div>
						</div>
						<div class="form-group">
							<label>Proses <span class="required-label">*</span></label>
							<select class="form-control"  name="proses" id="proses" >
								<option value="" >- Pilih Mesin -</option>
								
							</select>
						</div>
						<div class="form-group">
							<label>Status Part <span class="required-label">*</span></label>
							<select class="form-control " name="tipe" id="tipe" >
								<option value="" >- Pilih Material -</option>
								<option value="1" > IN </option>
								<option value="2" > OUT </option>
							</select>
						</div>
						<div class="form-group">
							<button type="submit"  class="btn btn-finish btn-primary" name="submit" value="Simpan">Simpan</button>
						</div>
					
						
					</form>
				</div>
			</div>
		</div>
	</div>
	<footer class="footer">
		<div class="container-fluid">
			<nav class="pull-left">
			</nav>
			<div class="copyright ml-auto">
				2020, by <a href="#">CAN</a>
			</div>				
		</div>
	</footer>
	<!--   Core JS Files   -->
	<script src="../assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="../assets/js/core/popper.min.js"></script>
	<script src="../assets/js/core/bootstrap.min.js"></script>

	<!-- jQuery UI -->
	<script src="../assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="../assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="../assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>

	<!-- Moment JS -->
	<script src="../assets/js/plugin/moment/moment.min.js"></script>

	<!-- Datatables -->
	<script src="../assets/js/plugin/datatables/datatables.min.js"></script>

	<!-- DateTimePicker -->
	<script src="../assets/js/plugin/datepicker/bootstrap-datetimepicker.min.js"></script>

	<!-- Bootstrap Tagsinput -->
	<script src="../assets/js/plugin/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>

	<!-- Bootstrap Wizard -->
	<script src="../assets/js/plugin/bootstrap-wizard/bootstrapwizard.js"></script>

	<!-- jQuery Validation -->
	<script src="../assets/js/plugin/jquery.validate/jquery.validate.min.js"></script>

	<!-- Select2 -->
	<script src="../assets/js/plugin/select2/select2.full.min.js"></script>

	<!-- Sweet Alert -->
	<script src="../assets/js/plugin/sweetalert/sweetalert.min.js"></script>


	<!-- Atlantis JS -->
	<script src="../assets/js/atlantis.min.js"></script>

	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
	<script >
		
		function scanqr(i){	

			var scanner = new Instascan.Scanner({ video: document.getElementById('preview'), scanPeriod: 5, mirror: false });
			scanner.addListener('scan',function(content){
				// alert(content);
				var part = content.split("-");
				document.getElementById('part').value = part[1];
				document.getElementById('idpart').value = part[0];
				document.getElementById('tipeproyek').value = part[2];
				scanner.stop();
				var select = document.getElementById("proses"),
				opt = document.createElement("option");
				var xmlhttp = new XMLHttpRequest();
				xmlhttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						var myObj = JSON.parse(this.responseText);
						for(index in myObj) {
						    select.options[select.options.length] = new Option(myObj[index]['mesin'], myObj[index]['id']);
						}
					}
				};
				if(part[2] == 'machining'){
					xmlhttp.open("GET", "getproses.php?id="+part[0]+"&&tipe=0", true);
				}else{
					xmlhttp.open("GET", "getproses.php?id="+part[0]+"&&tipe=1", true);
				}
				
				xmlhttp.send();

				//window.location.href=content;
			});
			Instascan.Camera.getCameras().then(function (cameras){
				if(cameras.length>0){
					// scanner.start(cameras[0]);
					if(i==1){
						// scanner.stop(cameras[0]);
						scanner.stop();
						scanner.start(cameras[1]);
						// scanner.stop(cameras[0]);
					}else{
						scanner.stop();
						scanner.start(cameras[0]);
					}
				}else{
					console.error('No cameras found.');
					alert('No cameras found.');
				}
			}).catch(function(e){
				console.error(e);
				alert(e);
			});

		}
		function detil(npk)
		{
			window.location.href = "detiloperator.php?idproyek="+npk;
		}
		
		
		$(document).ready(function() {
			$('#basic-datatables').DataTable({
			});

			$('#multi-filter-select').DataTable( {
				"pageLength": 5,
				initComplete: function () {
					this.api().columns().every( function () {
						var column = this;
						var select = $('<select class="form-control"><option value=""></option></select>')
						.appendTo( $(column.footer()).empty() )
						.on( 'change', function () {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
								);

							column
							.search( val ? '^'+val+'$' : '', true, false )
							.draw();
						} );

						column.data().unique().sort().each( function ( d, j ) {
							select.append( '<option value="'+d+'">'+d+'</option>' )
						} );
					} );
				}
			});

			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});

			var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

			$('#addRowButton').click(function() {
				$('#add-row').dataTable().fnAddData([
					$("#addName").val(),
					$("#addPosition").val(),
					$("#addOffice").val(),
					action
					]);
				$('#addRowModal').modal('hide');

			});
		});
		
	</script>	
</body>

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Dec 2019 18:39:00 GMT -->
</html>