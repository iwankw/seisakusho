<?php
	include "../koneksi.php";
	session_start();
	$pencarian = 0;
	// Delete
	$mydate=getdate(date("U"));
	if(isset($_POST['submit']))
	{
		$jml = count($_POST['checked_id']);
		$cb = $_POST['checked_id'];
		$proyek = $_POST['idproyek'];
		$date = date("Ymd");
		$datemonth = date("Ym");
		$view = mysqli_query($conn,"select  no_surat_jalan from surat_jalan as a join part as b on a.part = b.id where b.proyek = ".$proyek." order by a.id desc limit 1");
        $sel = mysqli_fetch_array($view);
        $nosurat = $sel['no_surat_jalan'];
        if($nosurat==0)
        {
        	$view = mysqli_query($conn,"select  no_surat_jalan from surat_jalan as a where LEFT(a.tgl_pengiriman,6) = '$datemonth' order by a.id desc limit 1");
	        $sel = mysqli_fetch_array($view);
	        $nosurat = $sel['no_surat_jalan']+1;
        	for($i=0;$i<$jml;$i++){
				$view = mysqli_query($conn,"insert into surat_jalan(part,tgl_pengiriman,no_surat_jalan,keterangan,status) values (".$cb[$i].",'$date','$nosurat','','Dibuat')") or  mysqli_error($conn);

			}
        }
	}
	$romawi;
	
	switch ("$mydate[month]") {
		case 'January':
			# code...
			$romawi = "I";
			break;
		case 'February':
			# code...
			$romawi = "II";
			break;
		case 'March':
			# code...
			$romawi = "III";
			break;
		case 'April':
			# code...
			$romawi = "IV";
			break;
		case 'May':
			# code...
			$romawi = "V";
			break;
		case 'June':
			# code...
			$romawi = "VI";
			break;
		case 'July':
			# code...
			$romawi = "VII";
			break;
		case 'August':
			# code...
			$romawi = "VIII";
			break;
		case 'September':
			# code...
			$romawi = "IX";
			break;
		case 'October':
			# code...
			$romawi = "X";
			break;
		case 'November':
			# code...
			$romawi = "XI";
			break;
		case 'December':
			# code...
			$romawi = "XII";
			break;
		
		default:
			# code...
			break;
	}
	
	
?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Dec 2019 18:38:13 GMT -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Project</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="../assets/img/icon.ico" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="../assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['../assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/atlantis.css">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="../assets/css/demo.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>
<body style="margin-left: 20;margin-right: 20px">
	<br><br>
	<div class="col-md-12 col-lg-12 col-s-12">
		<form action="suratjalannew.php" method="POST">
			<div class="row">
				<div class="col-md-8">
					<img src="../assets/img/logo.jpg" height="50px" width="300px">
					<br>
					<label style="word-wrap: break-word;width: 300px;">Pusat Niaga Cibodas Blok G 44 Jalan Gatot Subroto KM 3,5 Cibodas - Tangerang </label>
					<br><label>Telp. (021) 55738635/36</label><br>
					<label>Fax. (021) 55738637</label>
					<br><br>
					<?php  
						$view = mysqli_query($conn,"select  no_surat_jalan from surat_jalan as a join part as b on a.part = b.id where b.proyek = ".$proyek." order by a.id desc limit 1");
	        			$sel = mysqli_fetch_array($view);
					?>
					<label>SURAT JALAN NO. TGS/<?php echo "$mydate[year]".'/'.$romawi.'/'.$sel['no_surat_jalan']; ?></label>
				</div>
				<div class="col-md-4 pull-right">
					<label style="word-wrap: break-word;">Tangerang, <?php  echo "$mydate[weekday], $mydate[mday] $mydate[month] $mydate[year]"; ?> </label><br>
					<?php 
						$view = mysqli_query($conn,"select b.customer, b.alamat, a.nopo, a.codewo from proyek as a join customer as b on a.customer = b.id where a.id = ".$proyek."");
	        			$sel = mysqli_fetch_array($view);
					?>
					<h4 style="word-wrap: break-word;width: 300px;"><?php echo $sel['customer']; ?> </h4>
					<label style="word-wrap: break-word;width: 300px;"><?php echo $sel['alamat']; ?></label>
					<br><br>
					<label>PO NO. <?php echo $sel['nopo']; ?></label>&nbsp;
					<label>WO NO. <?php echo $sel['codewo']; ?></label>
				</div>

			</div>

			<div class="table-responsive">
				<table id="datatables" class="display table table-striped table-hover">
					<thead>
						<tr>
							<th style="text-align: center;">NO</th>
							<th style="text-align: center;">PART NAME</th>
							<th style="text-align: center;">QUANTITY</th>
							<th style="text-align: center;">KETERANGAN</th>
							<!-- <th style="text-align: center;width: 150px;">AKSI</th> -->
						</tr>
					</thead>
					<tbody>
						<?php
						 $a =0;
						 $ket=0;
							for($i=0;$i<$jml;$i++){
								$view = mysqli_query($conn, "select b.id, a.partname,a.qty,b.keterangan from part as a join surat_jalan as b on a.id = b.part where a.id = ".$cb[$i] ."");
	                        while ($row = mysqli_fetch_array($view)) {
	                        	$a++;
	                    ?>
	                        	<tr>
	                        		<td style="text-align: center;"><?php echo $a; ?>
	                        			<input type="hidden"  name="id<?php echo $ket; ?>"  value="<?php echo $row['id']; ?>"/>
	                        		</td>
									<td><?php echo $row['partname'];?> </td>
									<td style="text-align: center;"><?php echo $row['qty'];?> </td>
									<td>
										<?php 
											
											if(isset($row['keterangan']) && $row['keterangan']!='')
											{
												echo $row['keterangan'];
											}
											else
											{
										?>
												<input type="text" class="form-control" name="keterangan<?php echo $ket; ?>" >
										<?php
												
											}
											
										?>
									</td>
								</tr>
						<?php
							$ket++;
							}
	                       	}
	                    ?>
								
					</tbody>
				</table>
			</div>
			<br><br>
			<div class="row">
				<div class="col-md-4">
					<center><label>Tanda Terima</label></center>
				</div>
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<center><label>Hormat Kami,</label></center>
					<br><br><br>
					<center><label><?php echo $_SESSION['nama']; ?>	</label></center>
				</div>
			</div>
			<br><br><br>
			<center>
				<input type="hidden"  name="total"  value="<?php echo ($ket); ?>"/>
				<button class="btn btn-primary" type="submit" name="submit" value="cetak">
					Cetak Surat Jalan
				</button>
			</center>
		</form>
	</div>
	<script src="../assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="../assets/js/core/popper.min.js"></script>
	<script src="../assets/js/core/bootstrap.min.js"></script>

	<!-- jQuery UI -->
	<script src="../assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="../assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="../assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>

	<!-- Moment JS -->
	<script src="../assets/js/plugin/moment/moment.min.js"></script>

	<!-- Chart JS -->
	<script src="../assets/js/plugin/chart.js/chart.min.js"></script>

	<!-- jQuery Sparkline -->
	<script src="../assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

	<!-- Chart Circle -->
	<script src="../assets/js/plugin/chart-circle/circles.min.js"></script>

	<!-- Datatables -->
	<script src="../assets/js/plugin/datatables/datatables.min.js"></script>

	<!-- Bootstrap Notify -->
	<!-- <script src="../assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script> -->

	<!-- Bootstrap Toggle -->
	<script src="../assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js"></script>

	<!-- jQuery Vector Maps -->
	<script src="../assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
	<script src="../assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script>

	<!-- Google Maps Plugin -->
	<script src="../assets/js/plugin/gmaps/gmaps.js"></script>

	<!-- Dropzone -->
	<script src="../assets/js/plugin/dropzone/dropzone.min.js"></script>

	<!-- Fullcalendar -->
	<script src="../assets/js/plugin/fullcalendar/fullcalendar.min.js"></script>

	<!-- DateTimePicker -->
	<script src="../assets/js/plugin/datepicker/bootstrap-datetimepicker.min.js"></script>

	<!-- Bootstrap Tagsinput -->
	<script src="../assets/js/plugin/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>

	<!-- Bootstrap Wizard -->
	<script src="../assets/js/plugin/bootstrap-wizard/bootstrapwizard.js"></script>

	<!-- jQuery Validation -->
	<script src="../assets/js/plugin/jquery.validate/jquery.validate.min.js"></script>

	<!-- Summernote -->
	<script src="../assets/js/plugin/summernote/summernote-bs4.min.js"></script>

	<!-- Select2 -->
	<script src="../assets/js/plugin/select2/select2.full.min.js"></script>

	<!-- Sweet Alert -->
	<script src="../assets/js/plugin/sweetalert/sweetalert.min.js"></script>

	<!-- Owl Carousel -->
	<script src="../assets/js/plugin/owl-carousel/owl.carousel.min.js"></script>

	<!-- Magnific Popup -->
	<script src="../assets/js/plugin/jquery.magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- Atlantis JS -->
	<script src="../assets/js/atlantis.min.js"></script>

	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script >
		
		$(document).ready(function() {
			$('#datatables').DataTable({
				"searching":false,
				"info": false,
			});
		
	</script>	
	<!-- <script>
		window.print();
	</script> -->
</body>

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Dec 2019 18:39:00 GMT -->
</html>