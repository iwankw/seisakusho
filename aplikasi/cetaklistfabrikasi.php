<?php
	include "../koneksi.php";
	session_start();
	$id = $_GET['proyek'];
	$view = mysqli_query($conn,"select codewo, nama_proyek from proyek where id = $id");
    $sel = mysqli_fetch_array($view);
?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Dec 2019 18:38:13 GMT -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Project</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="../assets/img/icon.ico" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="../assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['../assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/atlantis.css">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="../assets/css/demo.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>
<body style="margin-left: 20;margin-right: 20px">
	<br><br>
	<div class="col-md-12 col-lg-12 col-s-12">
		<div class="row">
			<div class="col-sm-4">
				<img src="../assets/img/logo.jpg" height="50px" width="100px">
			</div>
			<div class="col-sm-6">
				<br>
				<h2>List Material - <?php echo $sel['nama_proyek']; ?> - <?php echo $sel['codewo']; ?></h2>
			</div>

		</div>
		<br>
		<br>
		<div class="row">
			<div class="col-md-12 col-lg-12 col-s-4">
				<div class="table-responsive">
					<table id="basic-datatables" class="display table table-striped table-hover" style="font-size: 12px;">
						<thead style="vertical-align: top;">
							<tr>
								<th style="text-align: center;" rowspan="2">NO</th>
								<th style="text-align: center;" rowspan="2">MATERIAL</th>
								<th style="text-align: center;" colspan="3">JIS & DIMENSI</th>
								<th style="text-align: center;" rowspan="2">QTY</th>
								<th style="text-align: center;" rowspan="2">NO GAMBAR</th>
							</tr>
							<tr>
								<th style="text-align: center;" >PANJANG</th>
								<th style="text-align: center;" >LEBAR</th>
								<th style="text-align: center;" >TINGGI</th>
							</tr>
						</thead>
						<tbody>
							<?php
		                        $view = mysqli_query($conn, "select * from list_material_fabrikasi as a   where a.proyek =". $_GET['proyek']."");
		                        $a =0;
		                        while ($row = mysqli_fetch_array($view)) {
		                        	$a++;
		                    ?>
		                        	<tr>
		                        		<td><?php echo $a;?></td>
										<td><?php echo $row['material'];?></td>
										<td><?php echo $row['panjang'];?> mm</td>
										<td><?php echo $row['lebar'];?> mm</td>
										<td><?php echo $row['tinggi'];?> mm</td>
										<td><?php echo $row['qty'];?> </td>
										<td><?php echo $row['no_gambar'];?> </td>
									</tr>
							<?php
		                       	}
		                    ?>
									
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		</div>
	</div>

	<script src="../assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="../assets/js/core/popper.min.js"></script>
	<script src="../assets/js/core/bootstrap.min.js"></script>

	<!-- jQuery UI -->
	<script src="../assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="../assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="../assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>

	<!-- Moment JS -->
	<script src="../assets/js/plugin/moment/moment.min.js"></script>

	<!-- Chart JS -->
	<script src="../assets/js/plugin/chart.js/chart.min.js"></script>

	<!-- jQuery Sparkline -->
	<script src="../assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

	<!-- Chart Circle -->
	<script src="../assets/js/plugin/chart-circle/circles.min.js"></script>

	<!-- Datatables -->
	<script src="../assets/js/plugin/datatables/datatables.min.js"></script>

	<!-- Bootstrap Notify -->
	<!-- <script src="../assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script> -->

	<!-- Bootstrap Toggle -->
	<script src="../assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js"></script>

	<!-- jQuery Vector Maps -->
	<script src="../assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
	<script src="../assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script>

	<!-- Google Maps Plugin -->
	<script src="../assets/js/plugin/gmaps/gmaps.js"></script>

	<!-- Dropzone -->
	<script src="../assets/js/plugin/dropzone/dropzone.min.js"></script>

	<!-- Fullcalendar -->
	<script src="../assets/js/plugin/fullcalendar/fullcalendar.min.js"></script>

	<!-- DateTimePicker -->
	<script src="../assets/js/plugin/datepicker/bootstrap-datetimepicker.min.js"></script>

	<!-- Bootstrap Tagsinput -->
	<script src="../assets/js/plugin/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>

	<!-- Bootstrap Wizard -->
	<script src="../assets/js/plugin/bootstrap-wizard/bootstrapwizard.js"></script>

	<!-- jQuery Validation -->
	<script src="../assets/js/plugin/jquery.validate/jquery.validate.min.js"></script>

	<!-- Summernote -->
	<script src="../assets/js/plugin/summernote/summernote-bs4.min.js"></script>

	<!-- Select2 -->
	<script src="../assets/js/plugin/select2/select2.full.min.js"></script>

	<!-- Sweet Alert -->
	<script src="../assets/js/plugin/sweetalert/sweetalert.min.js"></script>

	<!-- Owl Carousel -->
	<script src="../assets/js/plugin/owl-carousel/owl.carousel.min.js"></script>

	<!-- Magnific Popup -->
	<script src="../assets/js/plugin/jquery.magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- Atlantis JS -->
	<script src="../assets/js/atlantis.min.js"></script>

	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script >
		
		$(document).ready(function() {
			$('#datatables').DataTable({
				"searching":false,
				"info": false,
			});
		});
		
	</script>	
	<script>
		var css = '@page { size: 8.5in 5.5in;size: landscape; }',
		    head = document.head || document.getElementsByTagName('head')[0],
		    style = document.createElement('style');

		style.type = 'text/css';
		style.media = 'print';

		if (style.styleSheet){
		  style.styleSheet.cssText = css;
		} else {
		  style.appendChild(document.createTextNode(css));
		}

		head.appendChild(style);
		window.print();
		
	</script>
</body>

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Dec 2019 18:39:00 GMT -->
</html>