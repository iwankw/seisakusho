<?php
	include "../koneksi.php";
	session_start();
	$tahun = $_POST['tahun'];
	$bulan = $_POST['bulan'];
	
?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Dec 2019 18:38:13 GMT -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Project</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="../assets/img/icon.ico" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="../assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['../assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
		function carimaterial()
		{
			var material = document.getElementById('material').value;
			window.location.href = "indexpurchasing.php?idmaterial="+material;

		}
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/atlantis.css">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="../assets/css/demo.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>
<body>
	<br><br><br>
	<center><h2>Daftar Proyek</h2></center>
	<br><br><br>
	<div class="col-md-12 col-lg-12 col-s-12">
			<table id="datatables" class="display table table-striped table-hover" style="font-size: xx-small;">
				<thead>
					<tr>
						<!-- <th align="center"><input type="checkbox" id="selectall" name="all" value="all"/></th> 
						-->
						<th style="text-align: center;width: 15px;font-size: 12px;" rowspan="2" >NO</th>
						<th style="text-align: center;font-size: 12px;" rowspan="2">CUSTOMER</th>
						<th style="text-align: center;font-size: 12px;" rowspan="2">NAMA PROJECT</th>
						<th style="text-align: center;width: 70px;font-size: 12px;" rowspan="2">CODE WO</th>
						<th style="text-align: center;font-size: 12px;" rowspan="2">CS</th>
						<th style="text-align: center;font-size: 12px;" rowspan="2">KET</th>
						<th style="text-align: center;font-size: 12px;" colspan="2" >DELIVERY</th>
						
						<th style="text-align: center;font-size: 12px;" rowspan="2">NOMINAL</th>
						<th style="text-align: center;font-size: 12px;" rowspan="2">EST</th>
						<th style="text-align: center;font-size: 12px;" colspan="3" >COST</th>
						<th style="text-align: center;font-size: 12px;" rowspan="2" >PROFIT</th>
						<th style="text-align: center;font-size: 12px;" rowspan="2" >STATUS</th>
						<!-- <th style="text-align: center;width: 150px;">AKSI</th> -->
					</tr>
					<tr>
						<th style="text-align: center;font-size: 12px;" >DUE DATE</th>
						<th style="text-align: center;font-size: 12px;" >ACTUAL</th>
						<th style="text-align: center;font-size: 12px;" >MATERIAL</th>
						<th style="text-align: center;font-size: 12px;" >PROSES</th>
						<th style="text-align: center;font-size: 12px;" >SUBCONT</th>
						
					</tr>
				</thead>
				<tbody>
					
					<?php
						$filter = $tahun.$bulan;
						if($bulan=='' && $tahun != '')
						{
							$view = mysqli_query($conn, "select a.tipe_proyek, a.nama_proyek,a.codewo,a.cs,a.id, a.actualdeliv ,b.customer,a.keterangan,a.targetdeliv,a.po from proyek as a join customer as b on a.customer = b.id where LEFT(a.targetdeliv,4) =  '$tahun' and a.status = 'Aktif'");	
						}else if($tahun == '' && $bulan != '')
						{
							$view = mysqli_query($conn, "select a.tipe_proyek, a.nama_proyek,a.codewo,a.cs,a.id, a.actualdeliv ,b.customer,a.keterangan,a.targetdeliv,a.po from proyek as a join customer as b on a.customer = b.id where substring(a.targetdeliv,5,2) =  '$bulan' and a.status = 'Aktif'");	
						}
						else if($bulan=='' && $tahun == '')
						{
							$view = mysqli_query($conn, "select a.tipe_proyek, a.nama_proyek, a.codewo,a.cs,a.id, a.actualdeliv ,b.customer,a.keterangan,a.targetdeliv,a.po from proyek as a join customer as b on a.customer = b.id where a.status = 'Aktif'");	
						}
						else
						{
							$view = mysqli_query($conn, "select a.tipe_proyek, a.nama_proyek,a.codewo,a.cs,a.id, a.actualdeliv ,b.customer,a.keterangan,a.targetdeliv,a.po from proyek as a join customer as b on a.customer = b.id where LEFT(a.targetdeliv,6) =  '$filter' and a.status = 'Aktif'");	
						}
						
                        
                        $a =0;
                        while ($row = mysqli_fetch_array($view)) {
                    ?>
                    	<tr style="background-color: <?php $viewstatus = mysqli_query($conn, "select a.status from list_material as a JOIN part as b on a.part = b.id where proyek =".$row['id']."");
                    		$terkirim=0;
                    		$diterima=0;
                    		$supplier=0;
                    		while ($sel = mysqli_fetch_array($viewstatus)) {
                    			if($sel['status']=='Terkirim')
                    			{
                    				$terkirim++;
                    			}
                    			else if($sel['status']=='Diterima')
                    			{
                    				$diterima++;
                    			}else if($sel['status']=='Terkirim ke Supplier')
                    			{
                    				$supplier++;
                    			}
                    		}
                    		if($terkirim>0 && $diterima==0 && $supplier==0)
                    		{
                    			echo '#cc80ff';
                    		}if($supplier>0){
                    			echo '#8b80ff';
                    		
                    		}else if($terkirim==0 && $diterima>0 && $supplier==0){
                    			$viewstatus = mysqli_query($conn, "select a.status from part as a where proyek =".$row['id']."");
                    			$dibuat =0;
                    			$terkirim =0;
                    			while ($sel = mysqli_fetch_array($viewstatus)) {
                    				
                    				if($sel['status']=='Terkirim')
                    				{
                    					$terkirim++;
                    				}
                    				else if($sel['status']=='Dibuat')
                    				{
                    					$dibuat++;
                    				}
                    			}
                    			if ($terkirim==0) {
                    				echo '#80c6ff';
                    				# code...
                    			}else if($terkirim>0 && $dibuat==0)
                    			{
                    				echo '#1bff0f';
                    			}else if($dibuat>0 && $terkirim>0){
                    				echo '#00d176';
                    			}
                    			
// 							                                    			echo '#80c6ff';
                    		}
                    	?>">
                    		<td style="width: 15px;font-size: 11px"><?php $a++; echo $a;?></td>
							<td style="font-size: 11px;"><?php echo $row['customer'];?></td>
							<td style="font-size: 11px;"><?php echo $row['nama_proyek'];?></td>
							<td style="font-size: 11px;"><?php echo $row['codewo'];?> </td>
							<td style="font-size: 11px;"><?php echo $row['cs'];?> </td>
							<!-- <td><ul>
								<?php 
									
									$part_query = mysqli_query($conn, "select partname,qty from part where proyek = ".$row['id']."");
                                    while ($row_part = mysqli_fetch_array($part_query)) {
                    					echo "<li>".$row_part['partname']."(".$row_part['qty'].")</li>";
                    				}
                    			?> 
                    			</ul></td> -->
							<td style="font-size: 11px;"><?php echo $row['keterangan'];?> </td>
							<td style="font-size: 11px;"><?php echo date("d F Y", strtotime($row['targetdeliv']));
							?></td>
							
							<td style="font-size: 11px;"><?php if($row['actualdeliv']!=''){echo date("d F Y", strtotime($row['actualdeliv']));};
							?></td>
							<!-- Nominal -->
							<td style="font-size: 11px;"><?php echo 'Rp. '.number_format($row['po']);?></td>
							<!-- Estimasi -->
							<td style="font-size: 11px;">
								<?php
									$estimasi_material = 0;
	                                $aktual_material = 0;
	                                $estproses=0;
	                                $estsubcon=0;
	                                $actsubcon = 0;
									if($row['tipe_proyek'] == 'machining'){
										// query material estimasi
										$material_query = mysqli_query($conn, "select sum(harga) hargaest, sum(aktualharga) as hargaaktual from list_material as a join part as b on a.part = b.id where b.proyek =".$row['id']."");
										$row_material = mysqli_fetch_array($material_query);
										$estimasi_material = $row_material['hargaest'];
										$aktual_material = $row_material['hargaaktual'];
										// query material aktual

										// query proses internal
										
										$proses_query = mysqli_query($conn, "select sum(a.total) hargaint from estimasi as a join part as b on a.part = b.id JOIN proses as c on c.id = a.proses where c.subcon = 0 AND b.proyek = ".$row['id']."");
	                                    $row_proses = mysqli_fetch_array($proses_query);
                        				$estproses= $row_proses['hargaint'];
                        				// query subcon
                        				
										$subcon_query = mysqli_query($conn, "select sum(a.total) hargaint from estimasi as a join part as b on a.part = b.id JOIN proses as c on c.id = a.proses where c.subcon = 1 AND b.proyek = ".$row['id']."");
	                                    $row_subcon = mysqli_fetch_array($subcon_query);
                        				$estsubcon= $row_subcon['hargaint'];
                        				// Subcon Actual Query
                        				
                        				$subcon_query = mysqli_query($conn, "select sum(a.harga) hargaint from subcon as a join part as b on a.part = b.id where b.proyek = ".$row['id']."");
	                                    $row_subcon = mysqli_fetch_array($subcon_query);
                        				$actsubcon= $row_subcon['hargaint'];
                        				// Total Eestimasi
                        				$totalestimasi = ($estimasi_material + $estproses + $estsubcon) *1.3;
                        				echo 'Rp. '.number_format($totalestimasi);
									}else{
										// query material estimasi
										$material_query = mysqli_query($conn, "select sum(harga) hargaest, sum(aktualharga) as hargaaktual from list_material_fabrikasi as a where a.proyek =".$row['id']."");
										$row_material = mysqli_fetch_array($material_query);
										$estimasi_material = $row_material['hargaest'];
										$aktual_material = $row_material['hargaaktual'];
										// query material aktual

										// query proses internal
										
										$proses_query = mysqli_query($conn, "select sum(a.total) hargaint from estimasi as a join list_material_fabrikasi as b on a.material_fabrikasi = b.id JOIN proses as c on c.id = a.proses where c.subcon = 0 AND b.proyek = ".$row['id']."");
	                                    $row_proses = mysqli_fetch_array($proses_query);
                        				$estproses= $row_proses['hargaint'];
                        				// query subcon
                        				
										$subcon_query = mysqli_query($conn, "select sum(a.total) hargaint from estimasi as a join list_material_fabrikasi as b on a.material_fabrikasi = b.id JOIN proses as c on c.id = a.proses where c.subcon = 1 AND b.proyek = ".$row['id']."");
	                                    $row_subcon = mysqli_fetch_array($subcon_query);
                        				$estsubcon= $row_subcon['hargaint'];

                        				// Subcon Actual Query
                        				$subcon_query = mysqli_query($conn, "select sum(a.harga) hargaint from subcon as a join part as b on a.part = b.id where b.proyek = ".$row['id']."");
	                                    $row_subcon = mysqli_fetch_array($subcon_query);
                        				$actsubcon= $row_subcon['hargaint'];
                        				// Total Eestimasi
                        				$totalestimasi = ($estimasi_material + $estproses + $estsubcon) *1.3;
                        				echo 'Rp. '.number_format($totalestimasi);
									}
									
                				?>
                			</td>
                			<!-- Estimasi Material -->
							<td style="font-size: 11px;"><?php echo 'Rp. '.number_format($estimasi_material) ; ?></td>
							<!-- Estimasi Proses -->
							<td style="font-size: 11px;"><?php echo 'Rp. '.number_format($estproses) ; ?>
                			</td>
                			<!-- Estimasi Subcon -->
                			<td style="font-size: 11px;"><?php echo 'Rp. '.number_format($estsubcon) ; ?></td>
                			 <!-- Profit -->
                			<td style="font-size: 11px;"><?php
                				$profit= $row['po'] - ($aktual_material+$estproses+$actsubcon);
                				echo 'Rp. '.number_format($profit) ;
                			?></td>
                			<td style="font-size: 11px;"><?php 
                				$jumlah=0;
                				$terkirim=0;
								$material_query = mysqli_query($conn, "select status from part  where proyek = ".$row['id']."");
                                while ($row_material = mysqli_fetch_array($material_query)) {
                                	$jumlah++;
                                	if($row_material['status']=='Terkirim')
                                	{
                                		$terkirim++;
                                	}
                					
                				}
                				if($terkirim ==0)
                				{
                					echo "Proses";
                				}else if($terkirim==$jumlah)
                				{
                					echo "Selesai";
                				} else{
                					echo "Terkirim Sebagian";
                				}
                				?>
                				
                			</td>
						</tr>
				<?php
                   	}
                ?>
						
			</tbody>
		</table>
	</div>
	<footer class="footer">
		<div class="container-fluid">
			<nav class="pull-left">
			</nav>
			<div class="copyright ml-auto">
				2020, by <a href="#">CAN</a>
			</div>				
		</div>
	</footer>
		
		
		<!-- Custom template | don't include it in your project! -->
		
	<!--   Core JS Files   -->
	<script src="../assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="../assets/js/core/popper.min.js"></script>
	<script src="../assets/js/core/bootstrap.min.js"></script>

	<!-- jQuery UI -->
	<script src="../assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="../assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="../assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>

	<!-- Moment JS -->
	<script src="../assets/js/plugin/moment/moment.min.js"></script>

	<!-- Chart JS -->
	<script src="../assets/js/plugin/chart.js/chart.min.js"></script>

	<!-- jQuery Sparkline -->
	<script src="../assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

	<!-- Chart Circle -->
	<script src="../assets/js/plugin/chart-circle/circles.min.js"></script>

	<!-- Datatables -->
	<script src="../assets/js/plugin/datatables/datatables.min.js"></script>

	<!-- Bootstrap Notify -->
	<!-- <script src="../assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script> -->

	<!-- Bootstrap Toggle -->
	<script src="../assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js"></script>

	<!-- jQuery Vector Maps -->
	<script src="../assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
	<script src="../assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script>

	<!-- Google Maps Plugin -->
	<script src="../assets/js/plugin/gmaps/gmaps.js"></script>

	<!-- Dropzone -->
	<script src="../assets/js/plugin/dropzone/dropzone.min.js"></script>

	<!-- Fullcalendar -->
	<script src="../assets/js/plugin/fullcalendar/fullcalendar.min.js"></script>

	<!-- DateTimePicker -->
	<script src="../assets/js/plugin/datepicker/bootstrap-datetimepicker.min.js"></script>

	<!-- Bootstrap Tagsinput -->
	<script src="../assets/js/plugin/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>

	<!-- Bootstrap Wizard -->
	<script src="../assets/js/plugin/bootstrap-wizard/bootstrapwizard.js"></script>

	<!-- jQuery Validation -->
	<script src="../assets/js/plugin/jquery.validate/jquery.validate.min.js"></script>

	<!-- Summernote -->
	<script src="../assets/js/plugin/summernote/summernote-bs4.min.js"></script>

	<!-- Select2 -->
	<script src="../assets/js/plugin/select2/select2.full.min.js"></script>

	<!-- Sweet Alert -->
	<script src="../assets/js/plugin/sweetalert/sweetalert.min.js"></script>

	<!-- Owl Carousel -->
	<script src="../assets/js/plugin/owl-carousel/owl.carousel.min.js"></script>

	<!-- Magnific Popup -->
	<script src="../assets/js/plugin/jquery.magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- Atlantis JS -->
	<script src="../assets/js/atlantis.min.js"></script>

	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script >
		
		$(document).ready(function() {
			$('#datatables').DataTable({
				"searching":false,
				"info": false,
			});
		
	</script>	
	<script>
		window.print();
	</script>
</body>

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Dec 2019 18:39:00 GMT -->
</html>