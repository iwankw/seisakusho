<?php
	include "../koneksi.php";
	session_start();
	if($_SESSION['role']!= "Marketing" && $_SESSION['role']!= "Finance")
	{
		echo "<script>  window.location.href= '../index.php'; </script>";
	}
	$tahun = 0;
	$bulan = 0;
	// Delete
	if(isset($_GET['tahun']))
	{
		$tahun = $_GET['tahun'];
		$bulan = $_GET['bulan'];
	}
	if(isset($_GET['berhasil'])){
		echo '<script> swal({
        		title : "Data Berhasil Disimpan !",
				icon : "success",
				buttons: {        			
					confirm: {
						className : "btn btn-primary"
					}
				},
			}); </script>';
	}
?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Dec 2019 18:38:13 GMT -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Project</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="../assets/img/icon.ico" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="../assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['../assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
		function carimaterial()
		{
			var tahun = document.getElementById('tahun').value;
			var bulan = document.getElementById('bulan').value;
			window.location.href = "beranda.php?tahun="+tahun+"&&bulan="+bulan;

		}
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/atlantis.css">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="../assets/css/demo.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>
<body>
	<div class="wrapper sidebar_minimize">
		<div class="main-header">
			<!-- Logo Header -->
			<div class="logo-header" data-background-color="blue">
				
				<a href="indexpurchasing.php" class="logo">
					<img src="../assets/img/logo.jpg" width="160px" alt="navbar brand" class="navbar-brand">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
				<div class="nav-toggle">
					<button class="btn btn-toggle toggle-sidebar">
						<i class="icon-menu"></i>
					</button>
				</div>
			</div>
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
				
				<div class="container-fluid">
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item toggle-nav-search hidden-caret">
							<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
								<i class="fa fa-search"></i>
							</a>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link dropdown-toggle" href="#" id="notifDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-bell"></i>
								<span class="notification">4</span>
							</a>
							<ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
								<li>
									<div class="dropdown-title">You have 4 new notification</div>
								</li>
								<li>
									<div class="notif-scroll scrollbar-outer">
										<div class="notif-center">
											<a href="#">
												<div class="notif-icon notif-primary"> <i class="fa fa-user-plus"></i> </div>
												<div class="notif-content">
													<span class="block">
														New user registered
													</span>
													<span class="time">5 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-icon notif-success"> <i class="fa fa-comment"></i> </div>
												<div class="notif-content">
													<span class="block">
														Rahmad commented on Admin
													</span>
													<span class="time">12 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-img"> 
													<img src="../assets/img/profile2.jpg" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="block">
														Reza send messages to you
													</span>
													<span class="time">12 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-icon notif-danger"> <i class="fa fa-heart"></i> </div>
												<div class="notif-content">
													<span class="block">
														Farrah liked Admin
													</span>
													<span class="time">17 minutes ago</span> 
												</div>
											</a>
										</div>
									</div>
								</li>
								<li>
									<a class="see-all" href="javascript:void(0);">See all notifications<i class="fa fa-angle-right"></i> </a>
								</li>
							</ul>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<img src="../assets/img/profile.jpg" alt="..." class="avatar-img rounded-circle">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<div class="user-box">
											<div class="avatar-lg"><img src="../assets/img/profile.jpg" alt="image profile" class="avatar-img rounded"></div>
											<div class="u-text">
												<h4><?php echo $_SESSION['nama']; ?></h4>
												<a href="profile.php" class="btn btn-xs btn-secondary btn-sm">Ubah Profil</a>
											</div>
										</div>
									</li>
									<li>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="../index.php">Logout</a>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>

		<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="../assets/img/profile.jpg" alt="..." class="avatar-img rounded-circle">
						</div>
						<div class="info">
							<a data-toggle="collapse" href="#" aria-expanded="true">
								<span>
									<?php echo $_SESSION['nama']; ?>
									<span class="user-level"><?php echo $_SESSION['role']; ?></span>
									<span class="caret"></span>
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">
									<li>
										<a href="ubahprofil.php">
											<span class="link-collapse">Ubah Profil</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav nav-primary">
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-menu"></i>
							</span>
							<h4 class="text-section">Menu</h4>
						</li>
						<li class="nav-item active">
							<a href="beranda.php">
								<i class="fas fa-window-maximize"></i>
								<p>Beranda</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->

		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">Beranda</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="berandapurchasing.php">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="berandapurchasing.php">Beranda</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title pull-left">Beranda</div>
									<div class="pull-right">
										<!-- <a class="btn btn-primary" href="ubahproyek.php">Tambah Proyek</a> -->
									</div>
								</div>
								<div class="card-body">
									<form method="POST" action="cetakberanda.php">
										<div class="row">
											<div class="col-md-12 col-lg-12 col-s-4">
												<?php 
													$view = mysqli_query($conn, "select a.targetdeliv from proyek as a order by targetdeliv asc");	
													$date;
													$sel = mysqli_fetch_array($view);
													while ($sel = mysqli_fetch_array($view)) {
														$date =  $sel['targetdeliv'];
														break;
													}
													
												?>
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-4">
															<table border="1" >
																<tr>
																	<td colspan="2">Keterangan</td>
																</tr>
																<tr>
																	<td>Dibuat</td>
																	<td style="width: 50px"></td>
																</tr>
																<tr>
																	<td>List Material Dibuat</td>
																	<td style="background-color: #cc80ff;width: 50px"></td>
																</tr>
																<tr>
																	<td>Terkirim ke Supplier</td>
																	<td style="background-color: #8b80ff;width: 50px"></td>
																</tr>
																<tr>
																	<td>Material Diterima</td>
																	<td style="background-color: #80c6ff;width: 50px"></td>
																</tr>
																<tr>
																	<td>Terkirim Sebagian ke Customer</td>
																	<td style="background-color: #00d176;width: 50px"></td>
																</tr>
																<tr>
																	<td>Terkirim Semua ke Customer</td>
																	<td style="background-color: #1bff0f;width: 50px"></td>
																</tr>
															</table>
														</div>
														<div class="col-md-3">
															<select class="form-control form-control" onchange="carimaterial()" name="tahun" id="tahun" >
																<option value="" <?php if(isset($_GET['idubah'])){ if($selubah['vendor'] == ''){ echo "selected";}} ?>>- Pilih Tahun -</option>
																<?php 
																	$tahunlama = date("Y", strtotime($date));
																	$sekarang = date('Y');
																	for($i=$tahunlama;$i<=$sekarang;$i++)
																	{

										                        ?>
										                            	<option value="<?php echo $i;?>" <?php if(isset($_GET['tahun'])){ if($tahun == $i){ echo 'selected';}}; ?> ><?php echo $i;?></option>
										                        <?php
										                            }
																?>
															</select>
														</div>
														<div class="col-md-3">
															<select class="form-control form-control" onchange="carimaterial()" name="bulan" id="bulan" >
																<option value="" <?php if($bulan==''){  echo "selected";} ?>>- Pilih Bulan -</option>
																<option value="01" <?php if($bulan=='01'){  echo "selected";} ?>>Januari</option>
																<option value="02" <?php if($bulan=='02'){  echo "selected";} ?>>Februari</option>
																<option value="03" <?php if($bulan=='03'){  echo "selected";} ?>>Maret</option>
																<option value="04" <?php if($bulan=='04'){  echo "selected";} ?>>April</option>
																<option value="05" <?php if($bulan=='05'){  echo "selected";} ?>>Mei</option>
																<option value="06" <?php if($bulan=='06'){  echo "selected";} ?>>Juni</option>
																<option value="07" <?php if($bulan=='07'){  echo "selected";} ?>>Juli</option>
																<option value="08" <?php if($bulan=='08'){  echo "selected";} ?>>Agustus</option>
																<option value="09" <?php if($bulan=='09'){  echo "selected";} ?>>September</option>
																<option value="10" <?php if($bulan=='10'){  echo "selected";} ?>>Oktober</option>
																<option value="11" <?php if($bulan=='11'){  echo "selected";} ?>>November</option>
																<option value="12" <?php if($bulan=='12'){  echo "selected";} ?>>Desember</option>
															</select>
														</div>
														<div class="col-md-2">
															<button type="submit"  class="btn btn-finish btn-primary" name="submit" value="pdf">Cetak PDF</button>
														</div>
													</div>
												</div>
												<br/>
												<div class="table-responsive">
													<table id="datatables" class="display table table-striped table-hover" style="font-size: xx-small;">
														<thead>
															<tr>
																<!-- <th align="center"><input type="checkbox" id="selectall" name="all" value="all"/></th> 
																-->
																<th style="text-align: center;width: 15px;font-size: 12px;" rowspan="2" >NO</th>
																<th style="text-align: center;font-size: 12px;" rowspan="2">CUSTOMER</th>
																<th style="text-align: center;font-size: 12px;" rowspan="2">NAMA PROJECT</th>
																<th style="text-align: center;width: 70px;font-size: 12px;" rowspan="2">CODE WO</th>
																<th style="text-align: center;font-size: 12px;" rowspan="2">CS</th>
																<th style="text-align: center;font-size: 12px;" rowspan="2">KET</th>
																<th style="text-align: center;font-size: 12px;" colspan="2" >DELIVERY</th>
																
																<th style="text-align: center;font-size: 12px;" rowspan="2">NOMINAL</th>
																<th style="text-align: center;font-size: 12px;" rowspan="2">EST</th>
																<th style="text-align: center;font-size: 12px;" colspan="3" >COST</th>
																<th style="text-align: center;font-size: 12px;" rowspan="2" >PROFIT</th>
																<th style="text-align: center;font-size: 12px;" rowspan="2" >STATUS</th>
																<th style="text-align: center;font-size: 12px;" rowspan="2" >AKSI</th>
																<!-- <th style="text-align: center;width: 150px;">AKSI</th> -->
															</tr>
															<tr>
																<th style="text-align: center;font-size: 12px;" >DUE DATE</th>
																<th style="text-align: center;font-size: 12px;" >ACTUAL</th>
																<th style="text-align: center;font-size: 12px;" >MATERIAL</th>
																<th style="text-align: center;font-size: 12px;" >PROSES</th>
																<th style="text-align: center;font-size: 12px;" >SUBCONT</th>
																
															</tr>
														</thead>
														<tbody>
															
															<?php
																$filter = $tahun.$bulan;
																if($bulan=='' && $tahun != '')
																{
																	$view = mysqli_query($conn, "select a.tipe_proyek, a.nama_proyek,a.codewo,a.cs,a.id, a.actualdeliv ,b.customer,a.keterangan,a.targetdeliv,a.po from proyek as a join customer as b on a.customer = b.id where LEFT(a.targetdeliv,4) =  '$tahun' and a.status = 'Aktif'");	
																}else if($tahun == '' && $bulan != '')
																{
																	$view = mysqli_query($conn, "select a.tipe_proyek, a.nama_proyek,a.codewo,a.cs,a.id, a.actualdeliv ,b.customer,a.keterangan,a.targetdeliv,a.po from proyek as a join customer as b on a.customer = b.id where substring(a.targetdeliv,5,2) =  '$bulan' and a.status = 'Aktif'");	
																}
																else if($bulan=='' && $tahun == '')
																{
																	$view = mysqli_query($conn, "select a.tipe_proyek, a.tipe_proyek,  a.nama_proyek,a.codewo,a.cs,a.id, a.actualdeliv ,b.customer,a.keterangan,a.targetdeliv,a.po from proyek as a join customer as b on a.customer = b.id where a.status = 'Aktif'");	
																}
																else
																{
																	$view = mysqli_query($conn, "select a.tipe_proyek, a.nama_proyek,a.codewo,a.cs,a.id, a.actualdeliv ,b.customer,a.keterangan,a.targetdeliv,a.po from proyek as a join customer as b on a.customer = b.id where LEFT(a.targetdeliv,6) =  '$filter' and a.status = 'Aktif'");	
																}
																
							                                    
							                                    $a =0;
							                                    while ($row = mysqli_fetch_array($view)) {
							                                ?>
							                                    	<tr style="background-color: <?php $viewstatus = mysqli_query($conn, "select a.status from list_material as a JOIN part as b on a.part = b.id where proyek =".$row['id']."");
							                                    		$terkirim=0;
							                                    		$diterima=0;
							                                    		$supplier=0;
							                                    		while ($sel = mysqli_fetch_array($viewstatus)) {
							                                    			if($sel['status']=='Terkirim')
							                                    			{
							                                    				$terkirim++;
							                                    			}
							                                    			else if($sel['status']=='Diterima')
							                                    			{
							                                    				$diterima++;
							                                    			}else if($sel['status']=='Terkirim ke Supplier')
							                                    			{
							                                    				$supplier++;
							                                    			}
							                                    		}
							                                    		if($terkirim>0 && $diterima==0 && $supplier==0)
							                                    		{
							                                    			echo '#cc80ff';
							                                    		}if($supplier>0){
							                                    			echo '#8b80ff';
							                                    		
							                                    		}else if($terkirim==0 && $diterima>0 && $supplier==0){
							                                    			$viewstatus = mysqli_query($conn, "select a.status from part as a where proyek =".$row['id']."");
							                                    			$dibuat =0;
							                                    			$terkirim =0;
							                                    			while ($sel = mysqli_fetch_array($viewstatus)) {
							                                    				
							                                    				if($sel['status']=='Terkirim')
							                                    				{
							                                    					$terkirim++;
							                                    				}
							                                    				else if($sel['status']=='Dibuat')
							                                    				{
							                                    					$dibuat++;
							                                    				}
							                                    			}
							                                    			if ($terkirim==0) {
							                                    				echo '#80c6ff';
							                                    				# code...
							                                    			}else if($terkirim>0 && $dibuat==0)
							                                    			{
							                                    				echo '#1bff0f';
							                                    			}else if($dibuat>0 && $terkirim>0){
							                                    				echo '#00d176';
							                                    			}
							                                    			
// 							                                    			echo '#80c6ff';
							                                    		}
							                                    	?>">
							                                    		<td style="width: 15px;font-size: 11px"><?php $a++; echo $a;?></td>
																		<td style="font-size: 11px;"><?php echo $row['customer'];?></td>
																		<td style="font-size: 11px;"><?php echo $row['nama_proyek'];?></td>
																		<td style="font-size: 11px;"><?php echo $row['codewo'];?> </td>
																		<td style="font-size: 11px;"><?php echo $row['cs'];?> </td>
																		<!-- <td><ul>
																			<?php 
																				
																				$part_query = mysqli_query($conn, "select partname,qty from part where proyek = ".$row['id']."");
											                                    while ($row_part = mysqli_fetch_array($part_query)) {
							                                    					echo "<li>".$row_part['partname']."(".$row_part['qty'].")</li>";
							                                    				}
							                                    			?> 
							                                    			</ul></td> -->
																		<td style="font-size: 11px;"><?php echo $row['keterangan'];?> </td>
																		<td style="font-size: 11px;"><?php echo date("d F Y", strtotime($row['targetdeliv']));
																		?></td>
																		<td style="font-size: 11px;"><?php if($row['actualdeliv']!=''){echo date("d F Y", strtotime($row['actualdeliv']));};
																		?></td>
																		<!-- Nominal -->
																		<td style="font-size: 11px;"><?php echo 'Rp. '.number_format($row['po']);?></td>
																		<!-- Estimasi -->
																		<td style="font-size: 11px;">
																			<?php
																				$estimasi_material = 0;
												                                $aktual_material = 0;
												                                $estproses=0;
												                                $estsubcon=0;
												                                $actsubcon = 0;
																				if($row['tipe_proyek'] == 'machining'){
																					// query material estimasi
																					$material_query = mysqli_query($conn, "select sum(harga) hargaest, sum(aktualharga) as hargaaktual from list_material as a join part as b on a.part = b.id where b.proyek =".$row['id']."");
																					$row_material = mysqli_fetch_array($material_query);
																					$estimasi_material = $row_material['hargaest'];
																					$aktual_material = $row_material['hargaaktual'];
																					// query material aktual

																					// query proses internal
																					
																					$proses_query = mysqli_query($conn, "select sum(a.total) hargaint from estimasi as a join part as b on a.part = b.id JOIN proses as c on c.id = a.proses where c.subcon = 0 AND b.proyek = ".$row['id']."");
												                                    $row_proses = mysqli_fetch_array($proses_query);
								                                    				$estproses= $row_proses['hargaint'];
								                                    				// query subcon
								                                    				
																					$subcon_query = mysqli_query($conn, "select sum(a.total) hargaint from estimasi as a join part as b on a.part = b.id JOIN proses as c on c.id = a.proses where c.subcon = 1 AND b.proyek = ".$row['id']."");
												                                    $row_subcon = mysqli_fetch_array($subcon_query);
								                                    				$estsubcon= $row_subcon['hargaint'];
								                                    				// Subcon Actual Query
								                                    				
								                                    				$subcon_query = mysqli_query($conn, "select sum(a.harga) hargaint from subcon as a join part as b on a.part = b.id where b.proyek = ".$row['id']."");
												                                    $row_subcon = mysqli_fetch_array($subcon_query);
								                                    				$actsubcon= $row_subcon['hargaint'];
								                                    				// Total Eestimasi
								                                    				$totalestimasi = ($estimasi_material + $estproses + $estsubcon) *1.3;
								                                    				echo 'Rp. '.number_format($totalestimasi);
																				}
																				
						                                    				?>
						                                    			</td>
						                                    			<!-- Estimasi Material -->
																		<td style="font-size: 11px;"><?php echo 'Rp. '.number_format($estimasi_material) ; ?></td>
																		<!-- Estimasi Proses -->
																		<td style="font-size: 11px;"><?php echo 'Rp. '.number_format($estproses) ; ?>
						                                    			</td>
						                                    			<!-- Estimasi Subcon -->
						                                    			<td style="font-size: 11px;"><?php echo 'Rp. '.number_format($estsubcon) ; ?></td>
						                                    			 <!-- Profit -->
						                                    			<td style="font-size: 11px;"><?php
						                                    				$profit= $row['po'] - ($aktual_material+$estproses+$actsubcon);
						                                    				echo 'Rp. '.number_format($profit) ;
						                                    			?></td>
						                                    			<td style="font-size: 11px;"><?php 
						                                    				$jumlah=0;
						                                    				$terkirim=0;
																			$material_query = mysqli_query($conn, "select status from part  where proyek = ".$row['id']."");
										                                    while ($row_material = mysqli_fetch_array($material_query)) {
										                                    	$jumlah++;
										                                    	if($row_material['status']=='Terkirim')
										                                    	{
										                                    		$terkirim++;
										                                    	}
						                                    					
						                                    				}
						                                    				if($terkirim ==0)
						                                    				{
						                                    					echo "Proses";
						                                    				}else if($terkirim==$jumlah)
						                                    				{
						                                    					echo "Selesai";
						                                    				} else{
						                                    					echo "Terkirim Sebagian";
						                                    				}
						                                    				?>
						                                    				
						                                    			</td>
																		<td style="font-size: 11px;">
						                                    				<center><a class="btn btn-info" href="detilberanda.php?idproyek=<?php echo $row['id']; ?>" ><i class="fa fa-list" title="Detil Proyek"></i>
																			</a></center>
																		</td>
																	</tr>
															<?php
							                                   	}
							                                ?>
																	
														</tbody>
													</table>
												</div>
											</div>

										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">
					</nav>
					<div class="copyright ml-auto">
						2020, by <a href="#">CAN</a>
					</div>				
				</div>
			</footer>
		</div>
		
		
		<!-- Custom template | don't include it in your project! -->
		
		<!-- End Custom template -->
	</div>
	
	<!--   Core JS Files   -->
	<script src="../assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="../assets/js/core/popper.min.js"></script>
	<script src="../assets/js/core/bootstrap.min.js"></script>

	<!-- jQuery UI -->
	<script src="../assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="../assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="../assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>

	<!-- Moment JS -->
	<script src="../assets/js/plugin/moment/moment.min.js"></script>

	<!-- Chart JS -->
	<script src="../assets/js/plugin/chart.js/chart.min.js"></script>

	<!-- jQuery Sparkline -->
	<script src="../assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

	<!-- Chart Circle -->
	<script src="../assets/js/plugin/chart-circle/circles.min.js"></script>

	<!-- Datatables -->
	<script src="../assets/js/plugin/datatables/datatables.min.js"></script>

	<!-- Bootstrap Notify -->
	<!-- <script src="../assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script> -->

	<!-- Bootstrap Toggle -->
	<script src="../assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js"></script>

	<!-- jQuery Vector Maps -->
	<script src="../assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
	<script src="../assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script>

	<!-- Google Maps Plugin -->
	<script src="../assets/js/plugin/gmaps/gmaps.js"></script>

	<!-- Dropzone -->
	<script src="../assets/js/plugin/dropzone/dropzone.min.js"></script>

	<!-- Fullcalendar -->
	<script src="../assets/js/plugin/fullcalendar/fullcalendar.min.js"></script>

	<!-- DateTimePicker -->
	<script src="../assets/js/plugin/datepicker/bootstrap-datetimepicker.min.js"></script>

	<!-- Bootstrap Tagsinput -->
	<script src="../assets/js/plugin/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>

	<!-- Bootstrap Wizard -->
	<script src="../assets/js/plugin/bootstrap-wizard/bootstrapwizard.js"></script>

	<!-- jQuery Validation -->
	<script src="../assets/js/plugin/jquery.validate/jquery.validate.min.js"></script>

	<!-- Summernote -->
	<script src="../assets/js/plugin/summernote/summernote-bs4.min.js"></script>

	<!-- Select2 -->
	<script src="../assets/js/plugin/select2/select2.full.min.js"></script>

	<!-- Sweet Alert -->
	<script src="../assets/js/plugin/sweetalert/sweetalert.min.js"></script>

	<!-- Owl Carousel -->
	<script src="../assets/js/plugin/owl-carousel/owl.carousel.min.js"></script>

	<!-- Magnific Popup -->
	<script src="../assets/js/plugin/jquery.magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- Atlantis JS -->
	<script src="../assets/js/atlantis.min.js"></script>

	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script >
		$(document).ready(function(){
		    $('#selectall').on('click',function(){
		        if(this.checked){
		            $('.checkbox').each(function(){
		                this.checked = true;
		            });
		        }else{
		             $('.checkbox').each(function(){
		                this.checked = false;
		            });
		        }
		    });
			
		    $('.checkbox').on('click',function(){
		        if($('.checkbox:checked').length == $('.checkbox').length){
		            $('#selectall').prop('checked',true);
		        }else{
		            $('#selectall').prop('checked',false);
		        }
		    });
		});
		function ubah(npk)
		{
			window.location.href = "ubahproyek.php?idubah="+npk;
		}
		function material(npk)
		{
			window.location.href = "indexlistmaterial.php?idubah="+npk;
		}
		function konfirmasi(npk) {
			event.preventDefault(); // prevent form submit
			var form = event.target.form; // storing the form
			swal({
				title: "Apakah Anda yakin ingin menghapus data?",
				text : "",
				type: "warning",
				buttons:{
						confirm: {
							text : 'Hapus',
							className : 'btn btn-success'
						},
						cancel: {
							visible: true,
							text : 'Batal',
							className: 'btn btn-danger'
						}      			
						
					}
				}).then((willDelete) => {
						if (willDelete) {
							swal({
								title: "Data Berhasil Dihapus !",
								icon: "success",
								buttons: {
									confirm: {
										text: "OK",
										value: true,
										visible: true,
										className: "btn btn-success",
										closeModal: true
									}
								}
							}).then(
								function() {
									window.location.href = "indexproyek.php?iddelete="+npk;
								}
							);
							
						} else {
							swal("Data tidak dihapus", {
								buttons : {
									confirm : {
										className: 'btn btn-info'
									}
								}
							});
						}
					});
		}
		$(document).ready(function() {
			$('#datatables').DataTable({
				"searching":true,
				"info": false,
			});

			$('#multi-filter-select').DataTable( {
				"pageLength": 5,
				initComplete: function () {
					this.api().columns().every( function () {
						var column = this;
						var select = $('<select class="form-control"><option value=""></option></select>')
						.appendTo( $(column.footer()).empty() )
						.on( 'change', function () {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
								);

							column
							.search( val ? '^'+val+'$' : '', true, false )
							.draw();
						} );

						column.data().unique().sort().each( function ( d, j ) {
							select.append( '<option value="'+d+'">'+d+'</option>' )
						} );
					} );
				}
			});

			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,

			});

			var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

			$('#addRowButton').click(function() {
				$('#add-row').dataTable().fnAddData([
					$("#addName").val(),
					$("#addPosition").val(),
					$("#addOffice").val(),
					action
					]);
				$('#addRowModal').modal('hide');

			});
		});
		
	</script>	
</body>

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Dec 2019 18:39:00 GMT -->
</html>