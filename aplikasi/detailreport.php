<?php
include "../koneksi.php";
session_start();
if($_SESSION['role']!= "Quality Control")
{
	echo "<script>  window.location.href= '../index.php'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Project</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="../assets/img/icon.ico" type="image/x-icon"/>

	<script src="../assets/js/core/jquery.3.2.1.min.js"></script>
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<script src="../assets/js/core/bootstrap.min.js"></script>
	<script src="../assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['../assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<link rel="stylesheet" href="../assets/css/atlantis.css">
	<link rel="stylesheet" href="../assets/css/demo.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>
<body>
	<div class="wrapper ">
		<div class="main-header">
			<!-- Logo Header -->
			<div class="logo-header" data-background-color="blue">

				<a href="indexadmin.php" class="logo">
					<img src="../assets/img/logo.jpg" width="160px" alt="navbar brand" class="navbar-brand">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
				<div class="nav-toggle">
					<button class="btn btn-toggle toggle-sidebar">
						<i class="icon-menu"></i>
					</button>
				</div>
			</div>

			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">

				<div class="container-fluid">
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item toggle-nav-search hidden-caret">
							<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
								<i class="fa fa-search"></i>
							</a>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link dropdown-toggle" href="#" id="notifDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-bell"></i>
								<span class="notification">4</span>
							</a>
							<ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
								<li>
									<div class="dropdown-title">You have 4 new notification</div>
								</li>
								<li>
									<div class="notif-scroll scrollbar-outer">
										<div class="notif-center">
											<a href="#">
												<div class="notif-icon notif-primary"> <i class="fa fa-user-plus"></i> </div>
												<div class="notif-content">
													<span class="block">
														New user registered
													</span>
													<span class="time">5 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-icon notif-success"> <i class="fa fa-comment"></i> </div>
												<div class="notif-content">
													<span class="block">
														Rahmad commented on Admin
													</span>
													<span class="time">12 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-img"> 
													<img src="../assets/img/profile2.jpg" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="block">
														Reza send messages to you
													</span>
													<span class="time">12 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-icon notif-danger"> <i class="fa fa-heart"></i> </div>
												<div class="notif-content">
													<span class="block">
														Farrah liked Admin
													</span>
													<span class="time">17 minutes ago</span> 
												</div>
											</a>
										</div>
									</div>
								</li>
								<li>
									<a class="see-all" href="javascript:void(0);">See all notifications<i class="fa fa-angle-right"></i> </a>
								</li>
							</ul>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<img src="../assets/img/profile.jpg" alt="..." class="avatar-img rounded-circle">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<div class="user-box">
											<div class="avatar-lg"><img src="../assets/img/profile.jpg" alt="image profile" class="avatar-img rounded"></div>
											<div class="u-text">
												<h4><?php echo $_SESSION['nama']; ?></h4>
												<a href="profile.php" class="btn btn-xs btn-secondary btn-sm">Ubah Profil</a>
											</div>
										</div>
									</li>
									<li>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="../index.php">Logout</a>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>

		<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="../assets/img/profile.jpg" alt="..." class="avatar-img rounded-circle">
						</div>
						<div class="info">
							<a data-toggle="collapse" href="#" aria-expanded="true">
								<span>
									<?php echo $_SESSION['nama']; ?>
									<span class="user-level"><?php echo $_SESSION['role']; ?></span>
									<span class="caret"></span>
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">
									<li>
										<a href="ubahprofil.php">
											<span class="link-collapse">Ubah Profil</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav nav-primary">
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-menu"></i>
							</span>
							<h4 class="text-section">Menu</h4>
						</li>
						<li class="nav-item">
							<a href="berandaqc.php">
								<i class="fas fa-window-maximize"></i>
								<p>Beranda</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="indexprocessengineering.php">
								<i class="fas fa-window-maximize"></i>
								<p>Proses</p>
							</a>
						</li>
						<li class="nav-item active">
							<a href="indexreport.php">
								<i class="fas fa-window-maximize"></i>
								<p>Report</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="qualitycontrol.php">
								<i class="fas fa-window-maximize"></i>
								<p>Quality Control</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->

		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">Menu Part List & Control Progress</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Forms</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Part List & Control Progress</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title pull-left" style="align:center">PART LIST & CONTROL PROGRESS</div>
								</div>
								<div class="card-body">
									<form  method="POST">
										<div class="pull-right">

											<input type="hidden" name="idproyek" value="<?php echo $_GET['proyek']; ?>">
										</div>
										<br>

										<?php
											$query = mysqli_query($conn, "select nama_proyek, codewo, cs from proyek where id = ".$_GET['proyek']."");
											$row = mysqli_fetch_row($query);
										?>

										<table width="468" cellspacing="0" cellpadding="0" border="0">
											<tbody>
												<tr>
											        <td style="border: 0">
											        <h2><strong>NAME </strong></h2>
											        </td>
											        <td style="border: 0"><strong>: </strong></td>
											        <td style="border: 0"><strong><?php echo $row[0]; ?> </strong></td>
											    </tr>
											    <tr>
											        <td style="border: 0">
											        <h2><strong>WO </strong></h2>
											        </td>
											        <td style="border: 0"><strong>: </strong></td>
											        <td style="border: 0"><strong><?php echo $row[1]; ?> </strong></td>
											    </tr>
											    <tr>
											        <td style="border: 0">
											        <h2><strong>CS </strong></h2>
											        </td>
											        <td style="border: 0"><strong>: </strong></td>
											        <td style="border: 0"><strong><?php echo $row[2]; ?> </strong></td>
											    </tr>
											</tbody>
										</table>

										<br>
										<br>
										
										<!-- baru -->
										<div class="table-responsive">
										<table class="table table-bordered">
											<tr>
												<th style="text-align: center;width: 15px;" rowspan="2" >NO</th>
												<th style="text-align: center;" rowspan="2">PARTNAME</th>
												<th style="text-align: center;" rowspan="2">QTY</th>
												<th style="text-align: center;" rowspan="2">DESC</th>
												<th style="text-align: center;" colspan="8">PROSES</th>
												<th style="text-align: center;" rowspan="2">%</th>
												<th style="text-align: center;" rowspan="2">REMARK</th>
											</tr>
											<tr>
												<th style="text-align: center;">1</th>
												<th style="text-align: center;">2</th>
												<th style="text-align: center;">3</th>
												<th style="text-align: center;">4</th>
												<th style="text-align: center;">5</th>
												<th style="text-align: center;">6</th>
												<th style="text-align: center;">7</th>
												<th style="text-align: center;">8</th>
											</tr>
										<?php
										$view = mysqli_query($conn, "select b.id, a.codewo,a.cs,c.customer,b.partname,b.qty,b.status from proyek as a join part as b on a.id = b.proyek join customer as c on c.id = a.customer where a.id = ".$_GET['proyek']."");
										$part_count = $view->num_rows;
											$a=0;
											while ($row = mysqli_fetch_array($view)) {
												$row_total= 0;
												$grand_total = 0;
												$a++;
												?>
											<tr>
												<td style="width: 15px;" rowspan="2"><?php echo $a; ?></td>
												<td style="text-align: center;" rowspan="2"><?php echo $row['partname']; ?></td>
												<td style="text-align: center;" rowspan="2"><?php echo $row['qty']; ?></td>
												<td style="text-align: center;">M/C</td>
												<?php $proses = mysqli_query($conn, "select a.id, a.urutan, a.part, a.material_fabrikasi, a.proses, a.jam, a.total, b.singkatan from estimasi as a join proses as b on a.proses = b.id where a.urutan != '' and a.part = ".$row['id']."");
													while ($proses_row = mysqli_fetch_array($proses)){
												?>
												<td style="text-align: center;"><?php echo $proses_row['singkatan']; ?></td>
												<?php
													} ?>
												<?php
													for($i=0;$i<(8-$proses->num_rows);$i++){
												?>
													<td style="text-align: center;"></td>
												<?php
													}
												?>

												<?php
													$proses1 = mysqli_query($conn, "select id, urutan, part, material_fabrikasi, proses, jam, total from estimasi where urutan != '' and part = ".$row['id']."");
													while ($proses_row1 = mysqli_fetch_array($proses1)){
														$cek_flow1 = mysqli_query($conn, "select id from flow where estimasi = ".$proses_row1['id']."");
													$row_sub_total = $cek_flow1->num_rows;
													$row_total = $row_total + $row_sub_total;
													$proses_total = $proses1->num_rows;
													$qty = mysqli_query($conn, "select qty from part where id = ".$proses_row1['part']."");
													$row_qty = mysqli_fetch_row($qty);
													$qty_total = $row_qty[0];
													$persen = ($row_total/($proses_total*$qty_total))*100;
													$grand_total = $grand_total+$persen;
													}
												?>

												<td style="text-align: center;" rowspan="2"><?php echo round($persen, 2) ?>%</td>
												<td style="text-align: center;" rowspan="2"></td>
											</tr>
											<tr>
												<td style="text-align: center;">DATE</td>
												<?php $proses = mysqli_query($conn, "select id, urutan, part, material_fabrikasi, proses, jam, total from estimasi where urutan != '' and part = ".$row['id']."");
													while ($proses_row = mysqli_fetch_array($proses)){
														$cek_flow = mysqli_query($conn, "select id from flow where estimasi = ".$proses_row['id']."");
												?>
												<td style="text-align: center;"><?php echo $cek_flow->num_rows; ?></td>
												<?php
													} ?>
												<?php
													for($i=0;$i<(8-$proses->num_rows);$i++){
												?>
													<td style="text-align: center;"></td>
												<?php
													}
												?>
											</tr>
										<?php } ?>
											<tr>
												<td colspan="2">GRAND TOTAL</td>
												<td></td>
												<td colspan="9"></td>
												<td><?php echo round($grand_total/$part_count, 2); ?>%</td>
												<td></td>
											</tr>
											</table>
										</div>
										<!-- baru -->

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">
					</nav>
					<div class="copyright ml-auto">
						2020, by <a href="#">CAN</a>
					</div>				
				</div>
			</footer>
		</div>
	</div>
</body>
</html>