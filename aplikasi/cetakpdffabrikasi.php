<?php
	include "../koneksi.php";
	session_start();
	$pencarian = 0;
	// Delete
	if(isset($_GET['idmaterial']))
	{
		$pencarian = $_GET['idmaterial'];
		
	}
	if(isset($_POST['setsupplier']))
	{
		$cb = $_POST['checked_id2'];

		$id = explode(',',$cb);
		$supplier = $_POST['supplier'];
		$jml = count($id);
		$view=null;
		for($i=0;$i<$jml;$i++){
			$view = mysqli_query($conn,"update list_material_fabrikasi set supplier = '$supplier' where id= $id[$i] ") or  mysqli_error($conn);
		}
		echo "<script> alert('Data List Material Berhasil di Ubah !'); window.location.href= 'indexpurchasingfabrikasi.php'; </script>";
	}
	else if(isset($_POST['submit']))
	{

		if($_POST['submit'] == 'pdf')
		{
			$jml=0;
			if(isset($_POST['checked_id']))
			{
				$jml = count($_POST['checked_id']);
				// echo $jml;
				$cb = $_POST['checked_id'];
			}
			// print_r($cb);

		}

		else if($_POST['submit'] == 'ubah')
		{
			$supplier = $_POST['supplier'];
			$aktual = $_POST['aktual'];
			$id = $_POST['id'];
			if($aktual == '')
			{
				$view = mysqli_query($conn,"update list_material_fabrikasi set supplier = '$supplier', aktualharga = $aktual where id= $id ") or  mysqli_error($conn);
			}
			else
			{
				$view = mysqli_query($conn,"update list_material_fabrikasi set supplier = '$supplier', aktualharga = $aktual, status = 'Terkirim ke Supplier' where id= $id ") or  mysqli_error($conn);
			}
			
	    	if($view)
	    	{
	    		echo "<script> alert('Data List Material Berhasil di Ubah !'); window.location.href= 'indexpurchasingfabrikasi.php'; </script>";
	    	}
		}
		else if($_POST['submit'] == 'terima')
		{
			$jml = count($_POST['checked_id']);
			$cb = $_POST['checked_id'];
			$all = $_POST['all'];
			for($i=0;$i<$jml;$i++){
				$view = mysqli_query($conn,"update list_material_fabrikasi set status = 'Diterima' where id= $cb[$i] ") or  mysqli_error($conn);
			}
			echo "<script> alert('Data List Material Berhasil di Ubah !'); window.location.href= 'indexpurchasingfabrikasi.php'; </script>";
			
		}
		else if($_POST['submit'] == 'kesupplier')
		{
			
			$jml = count($_POST['checked_id']);
			$cb = $_POST['checked_id'];
			$all = $_POST['all'];
			for($i=0;$i<$jml;$i++){
				$view = mysqli_query($conn,"update list_material_fabrikasi set status = 'Terkirim ke Supplier' where id= $cb[$i] ") or  mysqli_error($conn);
			}
			echo "<script> alert('Data List Material Berhasil di Ubah !'); window.location.href= 'indexpurchasingfabrikasi.php'; </script>";
			
		}
			
	}
	if(isset($_GET['berhasil'])){
		echo '<script> swal({
        		title : "Data Berhasil Disimpan !",
				icon : "success",
				buttons: {        			
					confirm: {
						className : "btn btn-primary"
					}
				},
			}); </script>';
	}
	require_once("dompdf/autoload.inc.php");
	use Dompdf\Dompdf;
	$dompdf = new Dompdf();
	if(isset($_POST['submit'])&&$_POST['submit'] == 'pdf')
	{
		$html = '<!DOCTYPE html>
				<html>
				<head>
					<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css" >
					<style>
						@page { 
							margin-top: 10px;
							margin-left: 10px;
							margin-right: 10px;
							margin-bottom: 10px;
						}
						.table .th{
							border-color: #ebedf2 !important;
						    padding: 0 25px !important;
						    height: 30px;
						    vertical-align: middle !important;
						}
					</style>
				</head>
				<body style="margin-top:10px;line-height: 1.15;">
					<center><h2>LIST MATERIAL</h2></center>
					<br/><br/>
					<center><table  width="100%" border="1" style="font-size: 12px;"">
						<thead>
							<tr height="30px">
								<th style="text-align: center;font-size: 12px;">NO</th>
								<th style="text-align: center;font-size: 12px;">CODE WO</th>
								<th style="text-align: center;width: 70px;font-size: 12px;">CS</th>
								<th style="text-align: center;font-size: 12px;">PART NAME</th>
								<th style="text-align: center;font-size: 12px;">MATERIAL</th>
								<th style="text-align: center;font-size: 12px;">PANJANG</th>
								<th style="text-align: center;font-size: 12px;">LEBAR / ø </th>
								<th style="text-align: center;font-size: 12px;">TINGGI</th>
								<th style="text-align: center;font-size: 12px;">JUMLAH</th>
							</tr>
						</thead>
						<tbody>';
			$a=0;
			for($i=0;$i<$jml;$i++){
				$view = mysqli_query($conn,"select a.panjang,a.lebar, a.tinggi, a.id,a.supplier,c.cs ,a.status,c.codewo,c.customer,a.harga, a.material,a.qty from list_material_fabrikasi as a join  proyek as c on a.proyek = c.id   where a.status != 'Draft' and a.id = ".$cb[$i]." ");
				
				while ($row = mysqli_fetch_array($view)) {
	                 $a++;
	                $html .= " 	<tr>
	                        		<td style='text-align: center;'>". $a ."</td>
									<td style='text-align: center;'>". $row['codewo']."</td>
									<td style='text-align: center;'>".$row['cs']."</td>
									<td style='text-align: center;'>".$row['material']."</td>
									<td style='text-align: center;'>".$row['panjang']."</td>
									<td style='text-align: center;'>".$row['lebar']."</td>
									<td style='text-align: center;'>".$row['tinggi']."</td>
									<td style='text-align: center;'>".$row['qty']."</td>
								</tr>";
				}
			}
			$html .= '					
							</tbody>
						</table></center>
					</body>
					</html>'; 
		
		
		$dompdf->loadHtml($html);
		// Setting ukuran dan orientasi kertas
		$dompdf->setPaper('A4', 'potrait');
		// Rendering dari HTML Ke PDF
		$dompdf->render();
		// Melakukan output file Pdf
		$dompdf->stream('list_material_fabrikasi.pdf');
	}
		
?>
