<?php
include "../koneksi.php";
session_start();
if($_SESSION['role']!= "Quality Control")
{
	echo "<script>  window.location.href= '../index.php'; </script>";
}
if(isset($_GET['iddelete']))
{
	$view = mysqli_query($conn, "update part set status = 'OK' where id = ".$_GET['iddelete']."");
	echo "<script>  window.location.href= 'detailpart.php?proyek='".$_GET['proyek']."; </script>";
}
if(isset($_GET['idpart']))
{
	$selectData = mysqli_query($conn, "select * from part where id = ".$_GET["idpart"]."");
	while ($row = mysqli_fetch_array($selectData)) {
		$insertData = mysqli_query($conn, "INSERT INTO part (partname, qty, proyek, status, keterangan) VALUES ('".$row["partname"]."', '".$row["qty"]."', '".$row["proyek"]."', 'Dibuat', 'NG')");
		$query = mysqli_query($conn, "select a.tipe_proyek from proyek as a join part as b on b.proyek = a.id where b.id =".$_GET['idpart']."");
		$tipe =  mysqli_fetch_array($query);
		$query = mysqli_query($conn, "select a.id from part as a order by a.id desc LIMIT 1");
		$id_new =  mysqli_fetch_array($query);
		if($tipe['tipe_proyek']=='machining'){
			$view = mysqli_query($conn,"insert into list_material(part,status) values(".$id_new['id'].",'Draft') ") or mysqli_error($conn);
		}else if($tipe['tipe_proyek']=='fabrikasi'){
			$view = mysqli_query($conn,"insert into list_material_fabrikasi(proyek,status) values(".$row["proyek"].",'Draft') ") or mysqli_error($conn);
		}
			
	}
	echo "<script>  window.location.href= 'detailpart.php?proyek='".$_GET['proyek']."; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Project</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="../assets/img/icon.ico" type="image/x-icon"/>

	<script src="../assets/js/core/jquery.3.2.1.min.js"></script>
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<script src="../assets/js/core/bootstrap.min.js"></script>
	<script src="../assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['../assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<link rel="stylesheet" href="../assets/css/atlantis.css">
	<link rel="stylesheet" href="../assets/css/demo.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>
<body>
	<div class="wrapper ">
		<div class="main-header">
			<!-- Logo Header -->
			<div class="logo-header" data-background-color="blue">

				<a href="indexadmin.php" class="logo">
					<img src="../assets/img/logo.jpg" width="160px" alt="navbar brand" class="navbar-brand">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
				<div class="nav-toggle">
					<button class="btn btn-toggle toggle-sidebar">
						<i class="icon-menu"></i>
					</button>
				</div>
			</div>

			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">

				<div class="container-fluid">
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item toggle-nav-search hidden-caret">
							<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
								<i class="fa fa-search"></i>
							</a>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link dropdown-toggle" href="#" id="notifDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-bell"></i>
								<span class="notification">4</span>
							</a>
							<ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
								<li>
									<div class="dropdown-title">You have 4 new notification</div>
								</li>
								<li>
									<div class="notif-scroll scrollbar-outer">
										<div class="notif-center">
											<a href="#">
												<div class="notif-icon notif-primary"> <i class="fa fa-user-plus"></i> </div>
												<div class="notif-content">
													<span class="block">
														New user registered
													</span>
													<span class="time">5 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-icon notif-success"> <i class="fa fa-comment"></i> </div>
												<div class="notif-content">
													<span class="block">
														Rahmad commented on Admin
													</span>
													<span class="time">12 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-img"> 
													<img src="../assets/img/profile2.jpg" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="block">
														Reza send messages to you
													</span>
													<span class="time">12 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-icon notif-danger"> <i class="fa fa-heart"></i> </div>
												<div class="notif-content">
													<span class="block">
														Farrah liked Admin
													</span>
													<span class="time">17 minutes ago</span> 
												</div>
											</a>
										</div>
									</div>
								</li>
								<li>
									<a class="see-all" href="javascript:void(0);">See all notifications<i class="fa fa-angle-right"></i> </a>
								</li>
							</ul>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<img src="../assets/img/profile.jpg" alt="..." class="avatar-img rounded-circle">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<div class="user-box">
											<div class="avatar-lg"><img src="../assets/img/profile.jpg" alt="image profile" class="avatar-img rounded"></div>
											<div class="u-text">
												<h4><?php echo $_SESSION['nama']; ?></h4>
												<a href="profile.php" class="btn btn-xs btn-secondary btn-sm">Ubah Profil</a>
											</div>
										</div>
									</li>
									<li>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="../index.php">Logout</a>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>

		<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="../assets/img/profile.jpg" alt="..." class="avatar-img rounded-circle">
						</div>
						<div class="info">
							<a data-toggle="collapse" href="#" aria-expanded="true">
								<span>
									<?php echo $_SESSION['nama']; ?>
									<span class="user-level"><?php echo $_SESSION['role']; ?></span>
									<span class="caret"></span>
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">
									<li>
										<a href="ubahprofil.php">
											<span class="link-collapse">Ubah Profil</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav nav-primary">
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-menu"></i>
							</span>
							<h4 class="text-section">Menu</h4>
						</li>
						<li class="nav-item">
							<a href="berandaqc.php">
								<i class="fas fa-window-maximize"></i>
								<p>Beranda</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="indexprocessengineering.php">
								<i class="fas fa-window-maximize"></i>
								<p>Proses</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="indexreport.php">
								<i class="fas fa-window-maximize"></i>
								<p>Report</p>
							</a>
						</li>
						<li class="nav-item active">
							<a href="qualitycontrol.php">
								<i class="fas fa-window-maximize"></i>
								<p>Quality Control</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->

		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">Detail Part</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Forms</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Detail Part</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<div class="card">
								<div class="card-body">
									<form>
										<div class="pull-right">

											<input type="hidden" name="proyek" value="<?php echo $_GET['proyek']; ?>">
										</div>
										<br>
										<!-- <div id="result"></div> -->
										<div class="table-responsive">
											<table class="table table-bordered table-striped">
												<tr>
													<th style="text-align: center;width: 15px;"  >NO</th>
													<th style="text-align: center;">CUSTOMER</th>
													<th style="text-align: center;width: 70px;" >CODE WO</th>
													<th style="text-align: center;" >CS</th>
													<th style="text-align: center;" >PARTNAME</th>
													<th style="text-align: center;" >QTY</th>
													<th style="text-align: center;" >AKSI</th>
												</tr>
										<?php
											$view = mysqli_query($conn, "select b.id, a.codewo,a.cs,c.customer,b.partname,b.qty,b.status from proyek as a join part as b on a.id = b.proyek join customer as c on c.id = a.customer where a.id = ".$_GET['proyek']."");
											$a=0;
											while ($row = mysqli_fetch_array($view)) {
												$total1= 0;
												$a++;
										?>
												<tr>
													<td style="width: 15px;"><?php echo $a ?></td>
													<td><?php echo $row['customer'] ?></td>
													<td><?php echo $row['codewo'] ?></td>
													<td><?php echo $row['cs'] ?></td>
													<td><?php echo $row['partname'] ?></td>
													<td><?php echo $row['qty'] ?></td>
													<td><button class="btn btn-icon btn-round btn-info" onclick="update(<?php echo $row["id"] ?>, <?php echo $_GET['proyek'] ?>)" >
															<i class="fa fa-check" title="OK"></i>
														</button>
														<a type="button" name="edit" id="'<?php echo $row["id"] ?>'" class="btn btn-warning btn-xs add">Repair</a>
														<button class="btn btn-icon btn-round btn-info">
															<i class="fa flaticon-repeat" title="NG" onclick="updateNG(<?php echo $row["id"] ?>, <?php echo $_GET['proyek'] ?>)"></i>
														</button>

														</td>
												</tr>
										<?php } ?>
										</table></div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">
					</nav>
					<div class="copyright ml-auto">
						2020, by <a href="#">CAN</a>
					</div>				
				</div>
			</footer>
		</div>
	</div>
</body>
</html>
<div id="dynamic_field_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content" style="width:760px">
			<form method="post" id="add_name">
				<div class="modal-header">
					<h4 class="modal-title">Tambah Proses Part</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table" id="dynamic_field">
							
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="hidden_id" id="hidden_id" />
					<input type="hidden" name="action" id="action" value="insert" />
					<input type="submit" name="submit" id="submit" class="btn btn-info" value="Submit" />
				</div>
			</form>
		</div>
	</div>
</div>
<!-- Sweet Alert -->
<script src="../assets/js/plugin/sweetalert/sweetalert.min.js"></script>
<script>
	function update(npk, id) {
		event.preventDefault(); // prevent form submit
			var form = event.target.form; // storing the form
			swal({
				title: "Data Berhasil Diperbarui !",
				icon: "success",
				buttons: {
					confirm: {
						text: "OK",
						value: true,
						visible: true,
						className: "btn btn-success",
						closeModal: false
					}
				}
			}).then(
				function() {
					window.location.href = "detailpart.php?iddelete="+npk+"&proyek="+id;
				}
			);
		}

	function updateNG(npk, id) {
		event.preventDefault(); // prevent form submit
			var form = event.target.form; // storing the form
			swal({
				title: "Part Berhasil Ditambahkan !",
				icon: "success",
				buttons: {
					confirm: {
						text: "OK",
						value: true,
						visible: true,
						className: "btn btn-success",
						closeModal: false
					}
				}
			}).then(
				function() {
					window.location.href = "detailpart.php?idpart="+npk+"&proyek="+id;
				}
			);
		}
</script>
<script>
	$(document).ready(function(){

		var count = 1;

		function add_dynamic_input_field(count)
		{
			var button = '';
			if(count > 1)
			{
				button = '<button type="button" name="remove" id="'+count+'" class="btn btn-danger btn-xs remove">x</button>';
			}
			else
			{
				button = '<button type="button" name="add_more" id="add_more" class="btn btn-success btn-xs">+</button>';
			}
			output = '<tr id="row'+count+'">';
			output += '<td><input type="text" name="urutan[]" style="width:150px" placeholder="Tambahkan Urutan Proses" class="form-control urutan_list"/></td>';

			output += '<td><select name="proses[]" style="width:150px" class="form-control proses_list" required><option value="">-- Pilih Proses --</option><?php $view_proses1 = mysqli_query($conn,"select * from proses where status = 'Aktif'");
														while ($row_proses1 = mysqli_fetch_array($view_proses1)) {
																if($row_proses1['subcon']== '1'){
																	$subcon = 'SUBCON';
																}else{
																	$subcon = 'INTERNAL';
																}
																echo "<option value=".$row_proses1['id']." >".$row_proses1['mesin'].' - '.$subcon."</option>";
														}
													?>
												</select></td>';
			output += '<td><input type="text" name="jam[]" style="width:200px" placeholder="Tambahkan Waktu Proses" class="form-control jam_list"/></td>';
			output += '<td align="center">'+button+'</td></tr>';
			$('#dynamic_field').append(output);
		}

		function add_header()
		{
			output = '<tr><th>Urutan</th><th>Proses</th><th>Jumlah Jam</th></tr>';
			$('#dynamic_field').append(output);
		}

		$(document).on('click', '.add', function(){
			var id = $(this).attr("id");
			$('#dynamic_field').html('');
			add_header();
			add_dynamic_input_field(1);
			$('.modal-title').text('Tambah Proses Part');
			$('#action').val("insert");
			$('#submit').val('Submit');
			$('#add_name')[0].reset();
			$('#hidden_id').val(id);
			$('#dynamic_field_modal').modal('show');
		});

		$(document).on('click', '#add_more', function(){
			count = count + 1;
			add_dynamic_input_field(count);
		});

		$(document).on('click', '.remove', function(){
			var row_id = $(this).attr("id");
			$('#row'+row_id).remove();
		});

		$('#add_name').on('submit', function(event){
			event.preventDefault();
			var total_languages = 0;
			$('.urutan_list').each(function(){
				if($(this).val() != '')
				{
					total_languages = total_languages + 1;
				}
			});

			if(total_languages > 0)
			{
				var form_data = $(this).serialize();

				var action = $('#action').val();
				$.ajax({
					url:"action2.php",
					method:"POST",
					data:form_data,
					success:function(data)
					{
						if(action == 'insert')
						{
							alert("Data Inserted");
						}
						if(action == 'edit')
						{
							alert("Data Edited");
						}
						add_dynamic_input_field(1);
						$('#add_name')[0].reset();
						$('#dynamic_field_modal').modal('hide');
					}
				});
			}
			else
			{
				alert("Please Enter at least one process");
			}
		});

		$(document).on('click', '.edit', function(e){
			var id = $(this).attr("id");
			$.ajax({
				url:"select.php",
				method:"POST",
				data:{id:id},
				dataType:"JSON",
				success:function(data)
				{
					$('#dynamic_field').html(data.proses);
					$('#action').val('edit');
					$('.modal-title').text("Tambah Proses Part");
					$('#submit').val("Edit");
					$('#hidden_id').val(id);
					$('#dynamic_field_modal').modal('show');
				}
			});
		});

	});
</script>