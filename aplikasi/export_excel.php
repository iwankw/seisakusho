<?php
require_once "PHPExcel/PHPExcel.php";
include "../koneksi.php";

//instansiasi phpexcel
$objPHPExcel = new PHPExcel();

//set nama kolom
$objPHPExcel = PHPExcel_IOFactory::load("Report.xlsx");

$query = mysqli_query($conn, "select nama_proyek, codewo, cs from proyek where id = ".$_GET['proyek']."");
$row = mysqli_fetch_row($query);

$objPHPExcel->setActiveSheetIndex(0)
// ->setCellValue('A'.$i, $baris)
->setCellValue('K3', $row[1])
->setCellValue('E11', $row[0])
->setCellValue('E12', $row[1])
->setCellValue('E13', $row[2]);

$view = mysqli_query($conn, "select b.id, a.codewo,a.cs,c.customer,b.partname,b.qty,b.status from proyek as a join part as b on a.id = b.proyek join customer as c on c.id = a.customer where a.id = ".$_GET['proyek']."");
$part_count = $view->num_rows;
	$a=0;
	$b = 17;
	$c = 18;
	while ($row = mysqli_fetch_array($view)) {
		$row_total= 0;
		$grand_total = 0;
		$a++;
		$b++;
		$c++;

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('B'.$b, $a)
		->setCellValue('C'.$b, $a)
		->setCellValue('D'.$b, $row['partname'])
		->setCellValue('F'.$b, $row['qty'])
		->setCellValue('G'.$b, 'M/C')
		->mergeCells("B".$b.":B".($b+1))
		->mergeCells("C".$b.":C".($b+1))
		->mergeCells("D".$b.":E".($b+1))
		->mergeCells("F".$b.":F".($b+1));

		$proses = mysqli_query($conn, "select a.id, a.urutan, a.part, a.material_fabrikasi, a.proses, a.jam, a.total, b.singkatan from estimasi as a join proses as b on a.proses = b.id where a.urutan != '' and a.part = ".$row['id']."");
		$col = 7;
		while ($proses_row = mysqli_fetch_array($proses)){
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValueByColumnAndRow($col, $b, $proses_row['singkatan']);
			$col++;
		}

		$proses1 = mysqli_query($conn, "select id, urutan, part, material_fabrikasi, proses, jam, total from estimasi where urutan != '' and part = ".$row['id']."");
		while ($proses_row1 = mysqli_fetch_array($proses1)){
			$cek_flow1 = mysqli_query($conn, "select id from flow where estimasi = ".$proses_row1['id']."");
			$row_sub_total = $cek_flow1->num_rows;
			$row_total = $row_total + $row_sub_total;
			$proses_total = $proses1->num_rows;
			$qty = mysqli_query($conn, "select qty from part where id = ".$proses_row1['part']."");
			$row_qty = mysqli_fetch_row($qty);
			$qty_total = $row_qty[0];
			$persen = ($row_total/($proses_total*$qty_total))*100;
			$grand_total = $grand_total+$persen;
		}

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('P'.$b, round($persen, 2).'%')
		->setCellValue('G'.$c, 'DATE')
		->mergeCells("P".$b.":P".($b+1));

			$proses = mysqli_query($conn, "select id, urutan, part, material_fabrikasi, proses, jam, total from estimasi where urutan != '' and part = ".$row['id']."");
			$col1 = 7;
			while ($proses_row = mysqli_fetch_array($proses)){
				$cek_flow = mysqli_query($conn, "select id from flow where estimasi = ".$proses_row['id']."");
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueByColumnAndRow($col1, $c, $cek_flow->num_rows);
				$col1++;
			}
		$b++;
		$c++;
	}

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('B'.$c, 'GRAND TOTAL')
	->setCellValue('P'.$c, round($grand_total/$part_count, 2).'%')
	->mergeCells("B".$c.":E".$c);

	$styleArray = array(
	  'borders' => array(
	    'allborders' => array(
	      'style' => PHPExcel_Style_Border::BORDER_THIN
	    )
	  )
	);
	$objPHPExcel->getActiveSheet()->getStyle('B18:Q'.$c)->applyFromArray($styleArray);

$objPHPExcel->getActiveSheet()->setTitle('Report');

$objPHPExcel->setActiveSheetIndex(0);
$filename='Report1.xlsx';

$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Type: application/openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment;filename={$filename}");
header('Cache-Control: max-age=0');
$objWriter->save('php://output');

$objWriter->save($filename);
exit;

?>

<!-- <!DOCTYPE html>
<html lang="en">
<body>
	<div class="wrapper ">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title pull-left" style="align:center">PART LIST & CONTROL PROGRESS</div>
								</div>
								<div class="card-body">
									<form  method="POST">
										<div class="table-responsive">
										<table class="table table-bordered">
											<tr>
												<th style="text-align: center;width: 15px;" rowspan="2" >NO</th>
												<th style="text-align: center;" rowspan="2">PARTNAME</th>
												<th style="text-align: center;" rowspan="2">QTY</th>
												<th style="text-align: center;" rowspan="2">DESC</th>
												<th style="text-align: center;" colspan="8">PROSES</th>
												<th style="text-align: center;" rowspan="2">%</th>
												<th style="text-align: center;" rowspan="2">REMARK</th>
											</tr>
											<tr>
												<th style="text-align: center;">1</th>
												<th style="text-align: center;">2</th>
												<th style="text-align: center;">3</th>
												<th style="text-align: center;">4</th>
												<th style="text-align: center;">5</th>
												<th style="text-align: center;">6</th>
												<th style="text-align: center;">7</th>
												<th style="text-align: center;">8</th>
											</tr>
										<?php
										$view = mysqli_query($conn, "select b.id, a.codewo,a.cs,c.customer,b.partname,b.qty,b.status from proyek as a join part as b on a.id = b.proyek join customer as c on c.id = a.customer where a.id = ".$_GET['proyek']."");
										$part_count = $view->num_rows;
											$a=0;
											while ($row = mysqli_fetch_array($view)) {
												$row_total= 0;
												$grand_total = 0;
												$a++;
												?>
											<tr>
												<td style="width: 15px;" rowspan="2"><?php echo $a; ?></td>
												<td style="text-align: center;" rowspan="2"><?php echo $row['partname']; ?></td>
												<td style="text-align: center;" rowspan="2"><?php echo $row['qty']; ?></td>
												<td style="text-align: center;">M/C</td>
												<?php $proses = mysqli_query($conn, "select a.id, a.urutan, a.part, a.material_fabrikasi, a.proses, a.jam, a.total, b.singkatan from estimasi as a join proses as b on a.proses = b.id where a.urutan != '' and a.part = ".$row['id']."");
													while ($proses_row = mysqli_fetch_array($proses)){
												?>
												<td style="text-align: center;"><?php echo $proses_row['singkatan']; ?></td>
												<?php
													} ?>
												<?php
													for($i=0;$i<(8-$proses->num_rows);$i++){
												?>
													<td style="text-align: center;"></td>
												<?php
													}
												?>

												<?php
													$proses1 = mysqli_query($conn, "select id, urutan, part, material_fabrikasi, proses, jam, total from estimasi where urutan != '' and part = ".$row['id']."");
													while ($proses_row1 = mysqli_fetch_array($proses1)){
														$cek_flow1 = mysqli_query($conn, "select id from flow where estimasi = ".$proses_row1['id']."");
													$row_sub_total = $cek_flow1->num_rows;
													$row_total = $row_total + $row_sub_total;
													$proses_total = $proses1->num_rows;
													$qty = mysqli_query($conn, "select qty from part where id = ".$proses_row1['part']."");
													$row_qty = mysqli_fetch_row($qty);
													$qty_total = $row_qty[0];
													$persen = ($row_total/($proses_total*$qty_total))*100;
													$grand_total = $grand_total+$persen;
													}
												?>

												<td style="text-align: center;" rowspan="2"><?php echo round($persen, 2) ?>%</td>
												<td style="text-align: center;" rowspan="2"></td>
											</tr>
											<tr>
												<td style="text-align: center;">DATE</td>
												<?php $proses = mysqli_query($conn, "select id, urutan, part, material_fabrikasi, proses, jam, total from estimasi where urutan != '' and part = ".$row['id']."");
													while ($proses_row = mysqli_fetch_array($proses)){
														$cek_flow = mysqli_query($conn, "select id from flow where estimasi = ".$proses_row['id']."");
												?>
												<td style="text-align: center;"><?php echo $cek_flow->num_rows; ?></td>
												<?php
													} ?>
												<?php
													for($i=0;$i<(8-$proses->num_rows);$i++){
												?>
													<td style="text-align: center;"></td>
												<?php
													}
												?>
											</tr>
										<?php } ?>
											<tr>
												<td colspan="2">GRAND TOTAL</td>
												<td></td>
												<td colspan="9"></td>
												<td><?php echo round($grand_total/$part_count, 2); ?>%</td>
												<td></td>
											</tr>
											</table>
										</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html> -->